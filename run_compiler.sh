#!/bin/bash
# One argument must be given this argument is the file to be analyzed
set -e

if [ "$#" -eq 1 ]; then
	echo "Start - make in src/:" 
	echo ""

	cd ./src/
	make clean
	make

	echo ""
	echo "Make in prg/:" 
	echo ""

	cd ../prg/
	make clean
	make $1

	echo ""
	echo "Finish" 
else
	echo "Error no file name was given"
fi

