// Generated from /mnt/c/Users/Zann/Desktop/Faksic/2_Letnik/2_semester/Prevajalniki/domace_naloge/compiler-v2/src/pins/phase/lexan/PinsLexer.g4 by ANTLR 4.7.1

	package pins.phase.lexan;
	import pins.common.report.*;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class PinsLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		LP=1, RP=2, LBR=3, RBR=4, LB=5, RB=6, DOT=7, COLON=8, SEMICOLON=9, COMA=10, 
		AND=11, OR=12, NEGATE=13, EQUAL=14, NOTEQUAL=15, LT=16, GT=17, LE=18, 
		GE=19, MUL=20, DIVIDE=21, MOD=22, PLUS=23, MINUS=24, POW=25, ASSIGN=26, 
		CHAR=27, DEL=28, DO=29, ELSE=30, END=31, FUN=32, IF=33, INT=34, NEW=35, 
		THEN=36, TYP=37, VAR=38, VOID=39, WHERE=40, WHILE=41, NONE=42, NIL=43, 
		WHITESPACE=44, WHITESPACET=45, ID=46, INTCONST=47, CHARCONST=48, OPENCOMMENT=49, 
		CLOSEDCOMMENT=50, ONELINECOMMENT=51, ERROR=52;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"LP", "RP", "LBR", "RBR", "LB", "RB", "DOT", "COLON", "SEMICOLON", "COMA", 
		"AND", "OR", "NEGATE", "EQUAL", "NOTEQUAL", "LT", "GT", "LE", "GE", "MUL", 
		"DIVIDE", "MOD", "PLUS", "MINUS", "POW", "ASSIGN", "CHAR", "DEL", "DO", 
		"ELSE", "END", "FUN", "IF", "INT", "NEW", "THEN", "TYP", "VAR", "VOID", 
		"WHERE", "WHILE", "NONE", "NIL", "WHITESPACE", "WHITESPACET", "ID", "INTCONST", 
		"CHARCONST", "OPENCOMMENT", "CLOSEDCOMMENT", "ONELINECOMMENT", "ERROR", 
		"DigitFrom0", "DigitFrom1", "ZERO", "IdNoNumbers", "IdAll", "CHARS26_126", 
		"CHAREXCEPTION", "QUOTE", "OPENC"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'('", "')'", "'{'", "'}'", "'['", "']'", "'.'", "':'", "';'", "','", 
		"'&'", "'|'", "'!'", "'=='", "'!='", "'<'", "'>'", "'<='", "'>='", "'*'", 
		"'/'", "'%'", "'+'", "'-'", "'^'", "'='", "'char'", "'del'", "'do'", "'else'", 
		"'end'", "'fun'", "'if'", "'int'", "'new'", "'then'", "'typ'", "'var'", 
		"'void'", "'where'", "'while'", "'none'", "'nil'", null, null, null, null, 
		null, "'#{'", "'}#'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "LP", "RP", "LBR", "RBR", "LB", "RB", "DOT", "COLON", "SEMICOLON", 
		"COMA", "AND", "OR", "NEGATE", "EQUAL", "NOTEQUAL", "LT", "GT", "LE", 
		"GE", "MUL", "DIVIDE", "MOD", "PLUS", "MINUS", "POW", "ASSIGN", "CHAR", 
		"DEL", "DO", "ELSE", "END", "FUN", "IF", "INT", "NEW", "THEN", "TYP", 
		"VAR", "VOID", "WHERE", "WHILE", "NONE", "NIL", "WHITESPACE", "WHITESPACET", 
		"ID", "INTCONST", "CHARCONST", "OPENCOMMENT", "CLOSEDCOMMENT", "ONELINECOMMENT", 
		"ERROR"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	    @Override
		public LexAn.PrevToken nextToken() {
			return (LexAn.PrevToken) super.nextToken();
		}
		public void errorHappen(){
			if(LexAn.comment_stack.size() == 0){
				System.out.print("\u001B[31m" + "\n\nError at: " + getLine() + "." + getCharPositionInLine()  + " - " 
				+  getLine() + "." + (getCharPositionInLine() + getText().length() - 1) + 
				" Token at which the error occured: " + getText() + "\n");
				System.exit(1);
			}
		}


	public PinsLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "PinsLexer.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	@Override
	public void action(RuleContext _localctx, int ruleIndex, int actionIndex) {
		switch (ruleIndex) {
		case 44:
			WHITESPACET_action((RuleContext)_localctx, actionIndex);
			break;
		case 51:
			ERROR_action((RuleContext)_localctx, actionIndex);
			break;
		}
	}
	private void WHITESPACET_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 0:
			setCharPositionInLine((getCharPositionInLine()+8)-(getCharPositionInLine()%8) );
			break;
		}
	}
	private void ERROR_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 1:
			errorHappen();
			break;
		}
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\66\u0156\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"+
		"\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t"+
		" \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t"+
		"+\4,\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64"+
		"\t\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t"+
		"=\4>\t>\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3"+
		"\t\3\n\3\n\3\13\3\13\3\f\3\f\3\r\3\r\3\16\3\16\3\17\3\17\3\17\3\20\3\20"+
		"\3\20\3\21\3\21\3\22\3\22\3\23\3\23\3\23\3\24\3\24\3\24\3\25\3\25\3\26"+
		"\3\26\3\27\3\27\3\30\3\30\3\31\3\31\3\32\3\32\3\33\3\33\3\34\3\34\3\34"+
		"\3\34\3\34\3\35\3\35\3\35\3\35\3\36\3\36\3\36\3\37\3\37\3\37\3\37\3\37"+
		"\3 \3 \3 \3 \3!\3!\3!\3!\3\"\3\"\3\"\3#\3#\3#\3#\3$\3$\3$\3$\3%\3%\3%"+
		"\3%\3%\3&\3&\3&\3&\3\'\3\'\3\'\3\'\3(\3(\3(\3(\3(\3)\3)\3)\3)\3)\3)\3"+
		"*\3*\3*\3*\3*\3*\3+\3+\3+\3+\3+\3,\3,\3,\3,\3-\6-\u0102\n-\r-\16-\u0103"+
		"\3-\3-\3.\3.\3.\3.\3.\3/\3/\7/\u010f\n/\f/\16/\u0112\13/\3\60\3\60\7\60"+
		"\u0116\n\60\f\60\16\60\u0119\13\60\3\60\5\60\u011c\n\60\3\61\3\61\3\61"+
		"\5\61\u0121\n\61\3\61\3\61\3\62\3\62\3\62\3\62\3\62\3\63\3\63\3\63\3\63"+
		"\3\63\3\64\3\64\3\64\7\64\u0132\n\64\f\64\16\64\u0135\13\64\5\64\u0137"+
		"\n\64\3\64\3\64\3\65\3\65\3\65\3\66\3\66\3\67\3\67\38\68\u0143\n8\r8\16"+
		"8\u0144\39\39\3:\3:\3;\3;\3<\3<\3<\3<\5<\u0151\n<\3=\3=\3>\3>\2\2?\3\3"+
		"\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21"+
		"!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\34\67\359\36;\37= ?!"+
		"A\"C#E$G%I&K\'M(O)Q*S+U,W-Y.[/]\60_\61a\62c\63e\64g\65i\66k\2m\2o\2q\2"+
		"s\2u\2w\2y\2{\2\3\2\f\5\2\f\f\17\17\"\"\3\2\13\13\6\2\f\f\17\17AA}}\4"+
		"\2\f\f\17\17\3\2\62;\3\2\63;\3\2\62\62\5\2C\\aac|\6\2\62;C\\aac|\5\2\""+
		"(*]_\u0080\2\u0155\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13"+
		"\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2"+
		"\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2"+
		"!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3"+
		"\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2"+
		"\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E"+
		"\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2"+
		"\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\2[\3\2\2\2\2]\3\2\2\2"+
		"\2_\3\2\2\2\2a\3\2\2\2\2c\3\2\2\2\2e\3\2\2\2\2g\3\2\2\2\2i\3\2\2\2\3}"+
		"\3\2\2\2\5\177\3\2\2\2\7\u0081\3\2\2\2\t\u0083\3\2\2\2\13\u0085\3\2\2"+
		"\2\r\u0087\3\2\2\2\17\u0089\3\2\2\2\21\u008b\3\2\2\2\23\u008d\3\2\2\2"+
		"\25\u008f\3\2\2\2\27\u0091\3\2\2\2\31\u0093\3\2\2\2\33\u0095\3\2\2\2\35"+
		"\u0097\3\2\2\2\37\u009a\3\2\2\2!\u009d\3\2\2\2#\u009f\3\2\2\2%\u00a1\3"+
		"\2\2\2\'\u00a4\3\2\2\2)\u00a7\3\2\2\2+\u00a9\3\2\2\2-\u00ab\3\2\2\2/\u00ad"+
		"\3\2\2\2\61\u00af\3\2\2\2\63\u00b1\3\2\2\2\65\u00b3\3\2\2\2\67\u00b5\3"+
		"\2\2\29\u00ba\3\2\2\2;\u00be\3\2\2\2=\u00c1\3\2\2\2?\u00c6\3\2\2\2A\u00ca"+
		"\3\2\2\2C\u00ce\3\2\2\2E\u00d1\3\2\2\2G\u00d5\3\2\2\2I\u00d9\3\2\2\2K"+
		"\u00de\3\2\2\2M\u00e2\3\2\2\2O\u00e6\3\2\2\2Q\u00eb\3\2\2\2S\u00f1\3\2"+
		"\2\2U\u00f7\3\2\2\2W\u00fc\3\2\2\2Y\u0101\3\2\2\2[\u0107\3\2\2\2]\u010c"+
		"\3\2\2\2_\u011b\3\2\2\2a\u011d\3\2\2\2c\u0124\3\2\2\2e\u0129\3\2\2\2g"+
		"\u012e\3\2\2\2i\u013a\3\2\2\2k\u013d\3\2\2\2m\u013f\3\2\2\2o\u0142\3\2"+
		"\2\2q\u0146\3\2\2\2s\u0148\3\2\2\2u\u014a\3\2\2\2w\u0150\3\2\2\2y\u0152"+
		"\3\2\2\2{\u0154\3\2\2\2}~\7*\2\2~\4\3\2\2\2\177\u0080\7+\2\2\u0080\6\3"+
		"\2\2\2\u0081\u0082\7}\2\2\u0082\b\3\2\2\2\u0083\u0084\7\177\2\2\u0084"+
		"\n\3\2\2\2\u0085\u0086\7]\2\2\u0086\f\3\2\2\2\u0087\u0088\7_\2\2\u0088"+
		"\16\3\2\2\2\u0089\u008a\7\60\2\2\u008a\20\3\2\2\2\u008b\u008c\7<\2\2\u008c"+
		"\22\3\2\2\2\u008d\u008e\7=\2\2\u008e\24\3\2\2\2\u008f\u0090\7.\2\2\u0090"+
		"\26\3\2\2\2\u0091\u0092\7(\2\2\u0092\30\3\2\2\2\u0093\u0094\7~\2\2\u0094"+
		"\32\3\2\2\2\u0095\u0096\7#\2\2\u0096\34\3\2\2\2\u0097\u0098\7?\2\2\u0098"+
		"\u0099\7?\2\2\u0099\36\3\2\2\2\u009a\u009b\7#\2\2\u009b\u009c\7?\2\2\u009c"+
		" \3\2\2\2\u009d\u009e\7>\2\2\u009e\"\3\2\2\2\u009f\u00a0\7@\2\2\u00a0"+
		"$\3\2\2\2\u00a1\u00a2\7>\2\2\u00a2\u00a3\7?\2\2\u00a3&\3\2\2\2\u00a4\u00a5"+
		"\7@\2\2\u00a5\u00a6\7?\2\2\u00a6(\3\2\2\2\u00a7\u00a8\7,\2\2\u00a8*\3"+
		"\2\2\2\u00a9\u00aa\7\61\2\2\u00aa,\3\2\2\2\u00ab\u00ac\7\'\2\2\u00ac."+
		"\3\2\2\2\u00ad\u00ae\7-\2\2\u00ae\60\3\2\2\2\u00af\u00b0\7/\2\2\u00b0"+
		"\62\3\2\2\2\u00b1\u00b2\7`\2\2\u00b2\64\3\2\2\2\u00b3\u00b4\7?\2\2\u00b4"+
		"\66\3\2\2\2\u00b5\u00b6\7e\2\2\u00b6\u00b7\7j\2\2\u00b7\u00b8\7c\2\2\u00b8"+
		"\u00b9\7t\2\2\u00b98\3\2\2\2\u00ba\u00bb\7f\2\2\u00bb\u00bc\7g\2\2\u00bc"+
		"\u00bd\7n\2\2\u00bd:\3\2\2\2\u00be\u00bf\7f\2\2\u00bf\u00c0\7q\2\2\u00c0"+
		"<\3\2\2\2\u00c1\u00c2\7g\2\2\u00c2\u00c3\7n\2\2\u00c3\u00c4\7u\2\2\u00c4"+
		"\u00c5\7g\2\2\u00c5>\3\2\2\2\u00c6\u00c7\7g\2\2\u00c7\u00c8\7p\2\2\u00c8"+
		"\u00c9\7f\2\2\u00c9@\3\2\2\2\u00ca\u00cb\7h\2\2\u00cb\u00cc\7w\2\2\u00cc"+
		"\u00cd\7p\2\2\u00cdB\3\2\2\2\u00ce\u00cf\7k\2\2\u00cf\u00d0\7h\2\2\u00d0"+
		"D\3\2\2\2\u00d1\u00d2\7k\2\2\u00d2\u00d3\7p\2\2\u00d3\u00d4\7v\2\2\u00d4"+
		"F\3\2\2\2\u00d5\u00d6\7p\2\2\u00d6\u00d7\7g\2\2\u00d7\u00d8\7y\2\2\u00d8"+
		"H\3\2\2\2\u00d9\u00da\7v\2\2\u00da\u00db\7j\2\2\u00db\u00dc\7g\2\2\u00dc"+
		"\u00dd\7p\2\2\u00ddJ\3\2\2\2\u00de\u00df\7v\2\2\u00df\u00e0\7{\2\2\u00e0"+
		"\u00e1\7r\2\2\u00e1L\3\2\2\2\u00e2\u00e3\7x\2\2\u00e3\u00e4\7c\2\2\u00e4"+
		"\u00e5\7t\2\2\u00e5N\3\2\2\2\u00e6\u00e7\7x\2\2\u00e7\u00e8\7q\2\2\u00e8"+
		"\u00e9\7k\2\2\u00e9\u00ea\7f\2\2\u00eaP\3\2\2\2\u00eb\u00ec\7y\2\2\u00ec"+
		"\u00ed\7j\2\2\u00ed\u00ee\7g\2\2\u00ee\u00ef\7t\2\2\u00ef\u00f0\7g\2\2"+
		"\u00f0R\3\2\2\2\u00f1\u00f2\7y\2\2\u00f2\u00f3\7j\2\2\u00f3\u00f4\7k\2"+
		"\2\u00f4\u00f5\7n\2\2\u00f5\u00f6\7g\2\2\u00f6T\3\2\2\2\u00f7\u00f8\7"+
		"p\2\2\u00f8\u00f9\7q\2\2\u00f9\u00fa\7p\2\2\u00fa\u00fb\7g\2\2\u00fbV"+
		"\3\2\2\2\u00fc\u00fd\7p\2\2\u00fd\u00fe\7k\2\2\u00fe\u00ff\7n\2\2\u00ff"+
		"X\3\2\2\2\u0100\u0102\t\2\2\2\u0101\u0100\3\2\2\2\u0102\u0103\3\2\2\2"+
		"\u0103\u0101\3\2\2\2\u0103\u0104\3\2\2\2\u0104\u0105\3\2\2\2\u0105\u0106"+
		"\b-\2\2\u0106Z\3\2\2\2\u0107\u0108\t\3\2\2\u0108\u0109\b.\3\2\u0109\u010a"+
		"\3\2\2\2\u010a\u010b\b.\2\2\u010b\\\3\2\2\2\u010c\u0110\5q9\2\u010d\u010f"+
		"\5s:\2\u010e\u010d\3\2\2\2\u010f\u0112\3\2\2\2\u0110\u010e\3\2\2\2\u0110"+
		"\u0111\3\2\2\2\u0111^\3\2\2\2\u0112\u0110\3\2\2\2\u0113\u0117\5m\67\2"+
		"\u0114\u0116\5k\66\2\u0115\u0114\3\2\2\2\u0116\u0119\3\2\2\2\u0117\u0115"+
		"\3\2\2\2\u0117\u0118\3\2\2\2\u0118\u011c\3\2\2\2\u0119\u0117\3\2\2\2\u011a"+
		"\u011c\5o8\2\u011b\u0113\3\2\2\2\u011b\u011a\3\2\2\2\u011c`\3\2\2\2\u011d"+
		"\u0120\5y=\2\u011e\u0121\5u;\2\u011f\u0121\5w<\2\u0120\u011e\3\2\2\2\u0120"+
		"\u011f\3\2\2\2\u0121\u0122\3\2\2\2\u0122\u0123\5y=\2\u0123b\3\2\2\2\u0124"+
		"\u0125\7%\2\2\u0125\u0126\7}\2\2\u0126\u0127\3\2\2\2\u0127\u0128\b\62"+
		"\4\2\u0128d\3\2\2\2\u0129\u012a\7\177\2\2\u012a\u012b\7%\2\2\u012b\u012c"+
		"\3\2\2\2\u012c\u012d\b\63\4\2\u012df\3\2\2\2\u012e\u0136\7%\2\2\u012f"+
		"\u0133\n\4\2\2\u0130\u0132\n\5\2\2\u0131\u0130\3\2\2\2\u0132\u0135\3\2"+
		"\2\2\u0133\u0131\3\2\2\2\u0133\u0134\3\2\2\2\u0134\u0137\3\2\2\2\u0135"+
		"\u0133\3\2\2\2\u0136\u012f\3\2\2\2\u0136\u0137\3\2\2\2\u0137\u0138\3\2"+
		"\2\2\u0138\u0139\b\64\2\2\u0139h\3\2\2\2\u013a\u013b\13\2\2\2\u013b\u013c"+
		"\b\65\5\2\u013cj\3\2\2\2\u013d\u013e\t\6\2\2\u013el\3\2\2\2\u013f\u0140"+
		"\t\7\2\2\u0140n\3\2\2\2\u0141\u0143\t\b\2\2\u0142\u0141\3\2\2\2\u0143"+
		"\u0144\3\2\2\2\u0144\u0142\3\2\2\2\u0144\u0145\3\2\2\2\u0145p\3\2\2\2"+
		"\u0146\u0147\t\t\2\2\u0147r\3\2\2\2\u0148\u0149\t\n\2\2\u0149t\3\2\2\2"+
		"\u014a\u014b\t\13\2\2\u014bv\3\2\2\2\u014c\u014d\7^\2\2\u014d\u0151\7"+
		"^\2\2\u014e\u014f\7^\2\2\u014f\u0151\7)\2\2\u0150\u014c\3\2\2\2\u0150"+
		"\u014e\3\2\2\2\u0151x\3\2\2\2\u0152\u0153\7)\2\2\u0153z\3\2\2\2\u0154"+
		"\u0155\7}\2\2\u0155|\3\2\2\2\f\2\u0103\u0110\u0117\u011b\u0120\u0133\u0136"+
		"\u0144\u0150\6\b\2\2\3.\2\2\3\2\3\65\3";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}