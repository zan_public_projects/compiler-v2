lexer grammar PinsLexer;


@header {
	package pins.phase.lexan;
	import pins.common.report.*;
}

@members {
    @Override
	public LexAn.PrevToken nextToken() {
		return (LexAn.PrevToken) super.nextToken();
	}
	public void errorHappen(){
		if(LexAn.comment_stack.size() == 0){
			System.out.print("\u001B[31m" + "\n\nError at: " + getLine() + "." + getCharPositionInLine()  + " - " 
			+  getLine() + "." + (getCharPositionInLine() + getText().length() - 1) + 
			" Token at which the error occured: " + getText() + "\n");
			System.exit(1);
		}
	}
}
/*
	PARSER RULES

 */


/*
	LEXER RULES

	Fragment is a reusable piece of code if you use the same thing 2 times or 
	more use a fragment: fragment H          : ('H'|'h') ;
	If you don't include them in the lexer rules they have no effect
 */


// SYMBOLS
// Parentheses
LP         : '(' ;
RP         : ')' ;
LBR        : '{' ;
RBR        : '}' ;
LB         : '[' ;
RB         : ']' ;

// Dot operators
DOT        : '.' ;
COLON      : ':' ;
SEMICOLON  : ';' ;
COMA       : ',' ;

// Logic
AND	       : '&' ;
OR         : '|' ;
NEGATE     : '!' ;
EQUAL      : '==' ;
NOTEQUAL   : '!=' ;
LT         : '<' ;
GT         : '>' ;
LE         : '<=' ;
GE         : '>=' ;

// Math
MUL        : '*' ;
DIVIDE     : '/' ;
MOD        : '%' ;
PLUS       : '+' ;
MINUS      : '-' ;
POW        : '^' ;
ASSIGN     : '=' ;

	 
// KEYWORDS
CHAR       : 'char';
DEL        : 'del';
DO         : 'do';
ELSE       : 'else';
END        : 'end';
FUN        : 'fun';
IF         : 'if';
INT        : 'int';
NEW        : 'new';
THEN       : 'then';
TYP        : 'typ';
VAR        : 'var';
VOID       : 'void';
WHERE      : 'where';
WHILE      : 'while';
NONE       : 'none';
NIL        : 'nil';

// WHITESPACE
WHITESPACE : ([ \r\n]+) -> skip;
// skippa white space oz. jih ne izpise ('[\n\r\t]' | ' ') -> skip
WHITESPACET: ([\t]) {setCharPositionInLine((getCharPositionInLine()+8)-(getCharPositionInLine()%8) );} -> skip; 	

// ID-s
ID         : IdNoNumbers IdAll*;

// CONSTANTS
INTCONST   :  (DigitFrom1 DigitFrom0* | ZERO);
CHARCONST  :  QUOTE (CHARS26_126| CHAREXCEPTION) QUOTE; 			// ''[\\u0020-\\u007E]|'\[\\]';

// COMMENTS - MULTILINECOMMENT
OPENCOMMENT      : '#{'	-> channel(HIDDEN);
CLOSEDCOMMENT    : '}#' -> channel(HIDDEN);
//MULTILINEC : '#{' ( MULTILINEC | . | '\t')*? '}#' -> skip;

// SINGLE LINE COMMENT -this has to be under the multi line because it will else parse the multiLine as one line
ONELINECOMMENT   : ('#'(~[?\r\n{] ~[\r\n]* )?) -> skip; // 

ERROR : .  {errorHappen();};

fragment DigitFrom0    : [0-9];
fragment DigitFrom1    : [1-9];
fragment ZERO          : [0]+;
fragment IdNoNumbers   : [A-Za-z_];
fragment IdAll         : [A-Za-z0-9_];
fragment CHARS26_126   : [\u0020-\u0026\u0028-\u005B\u005D-\u007E];
fragment CHAREXCEPTION : '\\\\'|'\\\'';
fragment QUOTE         : '\'';
fragment OPENC         : '{';





