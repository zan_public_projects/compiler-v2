package pins.phase.seman;

import pins.common.report.*;
import pins.data.ast.tree.*;
import pins.data.ast.tree.decl.*;
import pins.data.ast.tree.expr.*;
import pins.data.ast.tree.stmt.*;
import pins.data.ast.tree.type.*;
import pins.data.ast.visitor.*;

/**
 * Name resolver.
 * 
 * Name resolver connects each node of a abstract syntax tree where a name is
 * used with the node where it is declared. The only exceptions are a record
 * field names which are connected with its declarations by type resolver. The
 * results of the name resolver are stored in
 * {@link pins.phase.seman.SemAn#declaredAt}.
 */
public class NameResolver extends AstFullVisitor<Object, NameResolver.Mode> {

	public enum Mode {
		HEAD, BODY
	}

	private SymbTable symbTable = new SymbTable();

	// profesor code
	public Object visit(AstTrees<?> trees, Mode mode) {
		
		for (AstTree tree : trees){
			if (tree instanceof AstTypeDecl){
				tree.accept(this, Mode.HEAD);
			}
		}

		for (AstTree tree : trees){
			if (tree instanceof AstTypeDecl){
				tree.accept(this, Mode.BODY);
			}
		}
			
		for (AstTree tree : trees) {
			if (tree instanceof AstVarDecl){
				tree.accept(this, mode);
			}
		}
					
		for (AstTree tree : trees) {
			if (tree instanceof AstFunDecl) {
				tree.accept(this, Mode.HEAD);
			}
		}
					
		for (AstTree tree : trees){
			if (tree instanceof AstFunDecl){
				tree.accept(this, Mode.BODY);
			}
		}

		return null;
	}

	@Override
	public Object visit(AstVarDecl varDecl, Mode mode) {
		// najprej obdelam levo stran deklaracije
		try {
			symbTable.ins(varDecl.name(), varDecl);
		} 
		catch (SymbTable.CannotInsNameException __) {
			throw new Report.Error(varDecl, "Cannot redefine '" + (varDecl.name()) + "' as a variable.");
		}
		// nato obdelam desno stran deklaracije
		varDecl.type().accept(this, mode);

		return null;
	}

	@Override
    public Object visit(AstFunDecl funDecl, Mode mode) {
        if (mode == Mode.HEAD) {
            try {
				for (AstTree tree : funDecl.pars()){
					tree.accept(this, Mode.BODY);
				}
				funDecl.type().accept(this, Mode.HEAD);
				
				funDecl.type().accept(this, Mode.BODY);
				symbTable.ins(funDecl.name(), funDecl);
			} 
			catch (SymbTable.CannotInsNameException __) {
                throw new Report.Error(funDecl, "Cannot redefine '" + (funDecl.name()) + "' as a variable.");
            }            
		}
		else if(mode == Mode.BODY){
			symbTable.newScope();
			for (AstTree tree : funDecl.pars()){
				tree.accept(this, Mode.HEAD);
			}
			funDecl.expr().accept(this, mode);
			symbTable.oldScope();
		}
		
        return null;
	}
	
	@Override
    public Object visit(AstTypeDecl typeDecl, Mode mode) {
        if (mode == Mode.HEAD) {
            try {
                symbTable.ins(typeDecl.name(), typeDecl);
			} 
			catch (SymbTable.CannotInsNameException __) {
                throw new Report.Error(typeDecl, "Cannot redefine '" + (typeDecl.name()) + "' as a variable.");
            }            
		}
		else if(mode == Mode.BODY){
			typeDecl.type().accept(this, mode);
		}
		
        return null;
	}

	@Override
    public Object visit(AstNameType nameType, Mode mode) {
		try {
			SemAn.declaredAt.put(nameType, symbTable.fnd(nameType.name()));
		} 
		catch (SymbTable.CannotFndNameException __) {
			throw new Report.Error(nameType, "Cannot find '" + (nameType.name()) + "' in symbol table.");
		}
        
        return null;
	}
	
	@Override
    public Object visit(AstNameExpr nameExpr, Mode mode) {
		try {
			SemAn.declaredAt.put(nameExpr, symbTable.fnd(nameExpr.name()));
		} 
		catch (SymbTable.CannotFndNameException __) {
			throw new Report.Error(nameExpr, "Cannot find '" + (nameExpr.name()) + "' in symbol table.");
		}            
	
        return null;
	}

	// parDecl
	@Override
    public Object visit(AstParDecl parDecl, Mode mode) {
        if (mode == Mode.HEAD) {
            try {
				// System.out.println("zivjo " + parDecl.name());
                symbTable.ins(parDecl.name(), parDecl);
			} 
			catch (SymbTable.CannotInsNameException __) {
                throw new Report.Error(parDecl, "Cannot redefine '" + (parDecl.name()) + "' as a variable.");
            }            
		}
		else if(mode == Mode.BODY){
			parDecl.type().accept(this, mode);
		}
		
        return null;
	}

	// stmt
	@Override
	public Object visit(AstStmtExpr stmtExpr, Mode mode) {
		for(AstTree tree : stmtExpr.stmts()){
			tree.accept(this, mode);
		}
		
		return null;
	}

	// stmt
	@Override
	public Object visit(AstWhileStmt stmtWhile, Mode mode) {
		stmtWhile.cond().accept(this, mode);

		for(AstTree tree : stmtWhile.bodyStmts()){
			tree.accept(this, mode);
		}
		
		return null;
	}

	// stmt
	@Override
	public Object visit(AstAssignStmt assignStmt, Mode mode) {
		assignStmt.src().accept(this, mode);
		assignStmt.dst().accept(this, mode);
		
		return null;
	}

	@Override
	public Object visit(AstBinExpr binExpr, Mode mode) {
		// if(mode == Mode.BODY){
		binExpr.fstExpr().accept(this, mode);
		binExpr.sndExpr().accept(this, mode);
		//}
		
		return null;
	}

	@Override
	public Object visit(AstArrType arrType, Mode mode) {
		arrType.elemType().accept(this, mode);
		arrType.numElems().accept(this, mode);
		
		return null;
	}

	// from the top baby
	@Override
	public Object visit(AstArrExpr arrExpr, Mode mode) {
		arrExpr.idx().accept(this, mode);
		arrExpr.arr().accept(this, mode);
		
		return null;
	}

	@Override
	public Object visit(AstCallExpr callExpr, Mode mode) {
		try {
			SemAn.declaredAt.put(callExpr, symbTable.fnd(callExpr.name()));
		} 
		catch (SymbTable.CannotFndNameException __) {
			throw new Report.Error(callExpr, "Cannot find '" + (callExpr.name()) + "' in symbol table.");
		}
		for(AstTree tree : callExpr.args()){
			tree.accept(this, mode);
		}
		
		return null;
	}

	@Override
	public Object visit(AstCastExpr castExpr, Mode mode) {
		castExpr.expr().accept(this, mode);
		castExpr.type().accept(this, mode);
		
		return null;
	}

	@Override
	public Object visit(AstPfxExpr pfxExpr, Mode mode) {
		pfxExpr.expr().accept(this, mode);
		
		return null;
	}

	@Override
	public Object visit(AstSfxExpr sfxExpr, Mode mode) {
		sfxExpr.expr().accept(this, mode);
		
		return null;
	}
	
	@Override
	public Object visit(AstWhereExpr whereExpr, Mode mode) {
		symbTable.newScope();
		
		whereExpr.decls().accept(this, mode);
		whereExpr.expr().accept(this, mode);

		symbTable.oldScope();
		
		return null;
	}

	// stmt
	@Override
	public Object visit(AstExprStmt exprStmt, Mode mode) {
		exprStmt.expr().accept(this, mode);
		
		return null;
	}

	@Override
	public Object visit(AstIfStmt ifStmt, Mode mode) {
		ifStmt.cond().accept(this, mode);

		for(AstTree tree : ifStmt.thenStmts()){
			tree.accept(this, mode);
		}
		for(AstTree tree : ifStmt.elseStmts()){
			tree.accept(this, mode);
		}
		
		return null;
	}

	// type
	public Object visit(AstPtrType ptrType, Mode mode) {
		ptrType.baseType().accept(this, mode);
		
		return null;
	}

	@Override
    public Object visit(AstAtomExpr atomExpr, Mode mode) {
        return null;
    }
}
