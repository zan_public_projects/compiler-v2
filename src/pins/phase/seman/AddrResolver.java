package pins.phase.seman;

import pins.data.ast.tree.expr.*;
import pins.data.ast.visitor.*;

/**
 * Address resolver.
 * 
 * The address resolver finds out which expressions denote lvalues and leaves
 * the information in {@link SemAn#isAddr}.
 */
public class AddrResolver extends AstFullVisitor<Object, Object> {

	// TODO - l value pregled - na koncu pdf pise kdaj je izraz naslovljiv
	// za ime spremenljivke ali parametra, ali expr pa strehca doloci naslov
	
	public Object visit(AstNameExpr nameExpr, Object mode){
		SemAn.isAddr.put(nameExpr, true);
		return null;
	}

	public Object visit(AstSfxExpr sfxExpr, Object mode) {
        SemAn.isAddr.put(sfxExpr, true);
        return null;
    }
}
