package pins.phase.seman;

import pins.data.semtype.*;
import pins.common.report.*;
import pins.data.ast.tree.*;
import pins.data.ast.tree.decl.*;
import pins.data.ast.tree.expr.*;
import pins.data.ast.tree.stmt.*;
import pins.data.ast.tree.type.*;
import pins.data.ast.visitor.*;

/**
 * Type resolver.
 * 
 * Type resolver computes the values of {@link SemAn#declaresType},
 * {@link SemAn#isType}, and {@link SemAn#ofType}.
 */
public class TypeResolver extends AstFullVisitor<SemType, TypeResolver.Mode> {

	public enum Mode {
		HEAD, BODY
	}

	// GENERAL PURPOSE

	@Override
	public SemType visit(AstTrees<?> trees, Mode mode) {
		for (AstTree tree : trees)
			if (tree instanceof AstTypeDecl)
				tree.accept(this, Mode.HEAD);
		for (AstTree tree : trees)
			if (tree instanceof AstTypeDecl)
				tree.accept(this, Mode.BODY);
		for (AstTree tree : trees)
			if (tree instanceof AstVarDecl)
				tree.accept(this, mode);
		for (AstTree tree : trees)
			if (tree instanceof AstFunDecl)
				tree.accept(this, Mode.HEAD);
		for (AstTree tree : trees)
			if (tree instanceof AstFunDecl)
				tree.accept(this, Mode.BODY);
				
		return null;
	}


	// TODO
	@Override
    public SemType visit(AstAtomExpr atomExpr, Mode mode) {
		switch (atomExpr.type().toString()) {
			case "VOID":
				SemAn.ofType.put(atomExpr, new SemVoid());
				break;
			case "CHAR":
				SemAn.ofType.put(atomExpr, new SemChar());
				break;
			case "INT":
				SemAn.ofType.put(atomExpr, new SemInteger());
				break;
			case "POINTER":
				SemAn.ofType.put(atomExpr, new SemPointer(new SemVoid()));
				break;
		
			default:
				throw new Report.Error(atomExpr.location(), "Non existing atom expression !!");
				// break;
		}

        return null;
    }

	@Override
	public SemType visit(AstAtomType atomType, Mode mode) {
		switch (atomType.type().toString()) {
			case "VOID":
				SemAn.isType.put(atomType, new SemVoid());
				break;
			case "CHAR":
				SemAn.isType.put(atomType, new SemChar());
				break;
			case "INT":
				SemAn.isType.put(atomType, new SemInteger());
				break;
		
			default:
				throw new Report.Error(atomType.location(), "Non existing atom type !!");
				// break;
		}
		return null;
	}

	@Override
	public SemType visit(AstArrType arrType, Mode mode) {
		arrType.elemType().accept(this, mode);
		arrType.numElems().accept(this, mode);

		try {
			AstAtomExpr numElemsAtom = (AstAtomExpr) arrType.numElems();
			long numElements = Long.parseLong(numElemsAtom.value());
			if (numElements < 1){
				throw new Report.Error(arrType.location(), "Array size should be bigger than 0 !!");
				// System.exit(1);
			}
			if(SemAn.isType.get(arrType.elemType()) instanceof SemVoid){
				throw new Report.Error(arrType.location(), "Array type can't be void !!");
			}	

			SemAn.isType.put(arrType, new SemArray(SemAn.isType.get(arrType.elemType()), numElements));
		}
		catch (ClassCastException e) {
			throw new Report.Error(arrType.location(), "The array does not contain an int-const !!");
		}
		return null;
	}

	@Override
	public SemType visit(AstNameType nameType, Mode mode) {
		AstDecl typeOf = SemAn.declaredAt.get(nameType);

		if(typeOf instanceof AstTypeDecl){
			SemName name = new SemName(nameType.name());
			name.define(SemAn.declaresType.get((AstTypeDecl)typeOf));
			
			if(SemAn.isType.get(((AstTypeDecl)SemAn.declaredAt.get(nameType)).type()) == null){
				SemAn.isType.put(nameType, name);
			}
			else{
				SemAn.isType.put(nameType, SemAn.isType.get(((AstTypeDecl)SemAn.declaredAt.get(nameType)).type()));
			}
			// return SemAn.isType.get(nameType); <- just in case 
		}
		else{
			if(SemAn.declaredAt.get(nameType) instanceof AstVarDecl){
				throw new Report.Error(nameType.location(), "Can't assign variable to variable !!");
			}
		}
		return null;
	}


	public SemType visit(AstVarDecl varDecl, Mode mode) {
		varDecl.type().accept(this, mode);

		return null;
	}

	@Override
    public SemType visit(AstFunDecl funDecl, Mode mode) {
        if (mode == Mode.HEAD) {
			for (AstTree tree : funDecl.pars()){
				tree.accept(this, Mode.BODY);
			}
			funDecl.type().accept(this, Mode.HEAD);
			funDecl.type().accept(this, Mode.BODY);	
		}
		else if(mode == Mode.BODY){
			for (AstTree tree : funDecl.pars()){
				tree.accept(this, Mode.HEAD);
			}
			funDecl.expr().accept(this, mode);

			SemType funExpr = SemAn.ofType.get(funDecl.expr());
			SemType funType = SemAn.isType.get(funDecl.type());

			if(!(funExpr instanceof SemInteger || funExpr instanceof SemChar || funExpr instanceof SemPointer || funExpr instanceof SemVoid)){
				throw new Report.Error(funDecl.location(), "Function return expression isn't of type int, char or pointer !!");
			}
			if(!(funType instanceof SemInteger || funType instanceof SemChar || funType instanceof SemPointer || funType instanceof SemVoid )){
				throw new Report.Error(funDecl.location(), "Function type isn't of type int, char or pointer !!");
			}

            if (SemAn.isType.get(funDecl.type()).getClass() != SemAn.ofType.get(funDecl.expr()).getClass()) {
				throw new Report.Error(funDecl.location(), "Expresion type doesn't match function return type !!");
			}
		}

        return null;
	}
	
	@Override
    public SemType visit(AstTypeDecl typeDecl, Mode mode) {
        if (mode == Mode.HEAD) { 
			SemAn.declaresType.put(typeDecl, new SemName(typeDecl.name()));
		}
		else if(mode == Mode.BODY){
			typeDecl.type().accept(this, mode);

			SemName temp_name = SemAn.declaresType.get(typeDecl);
			temp_name.define(SemAn.isType.get(typeDecl.type()));		
		}
		
        return null;
	}

	
	@Override
    public SemType visit(AstNameExpr nameExpr, Mode mode) {      
		if(SemAn.declaredAt.get(nameExpr) instanceof AstTypeDecl) {
			SemAn.ofType.put(nameExpr, SemAn.declaresType.get((AstTypeDecl) SemAn.declaredAt.get(nameExpr)));
		}
		else if(SemAn.declaredAt.get(nameExpr) instanceof AstVarDecl){
			SemAn.ofType.put(nameExpr, SemAn.isType.get(((AstVarDecl) SemAn.declaredAt.get(nameExpr)).type() ));
		}
		else if(SemAn.declaredAt.get(nameExpr) instanceof AstParDecl){
			SemAn.ofType.put(nameExpr, SemAn.isType.get(((AstParDecl) SemAn.declaredAt.get(nameExpr)).type() ));
		}
		
        return null;
	}

	// parDecl
	@Override
    public SemType visit(AstParDecl parDecl, Mode mode) {
		parDecl.type().accept(this, mode);
		SemType parType = SemAn.isType.get(parDecl.type());
		
		if(!(parType instanceof SemInteger || parType instanceof SemChar || parType instanceof SemPointer)){
			throw new Report.Error(parDecl.location(), "Parameter is of incorrect type, " +
			" only ints, chars and pointers can be function parameters !!");
		}
		
        return null;
	}

	// stmt
	@Override
	public SemType visit(AstStmtExpr stmtExpr, Mode mode) {
		for(AstTree tree : stmtExpr.stmts()){
			tree.accept(this, mode);
		}

		SemType stmtsType =  SemAn.ofType.get(stmtExpr.stmts().get(stmtExpr.stmts().size() - 1));
		SemAn.ofType.put(stmtExpr, stmtsType);
		
		return null;
	}

	// stmt
	@Override
	public SemType visit(AstWhileStmt stmtWhile, Mode mode) {
		stmtWhile.cond().accept(this, mode);
		
		for(AstTree tree : stmtWhile.bodyStmts()){
			tree.accept(this, mode);
		}

		SemType condition = SemAn.ofType.get(stmtWhile.cond());

		if(!(condition instanceof SemInteger)){
			throw new Report.Error(stmtWhile.location(), "Condition of while must be of integer type !!");
		}

		for(int i = 0; i < stmtWhile.bodyStmts().size(); i++){
			SemType bodyStmt = SemAn.ofType.get(stmtWhile.bodyStmts().get(i));
			if(!(bodyStmt instanceof SemVoid)){
				throw new Report.Error(stmtWhile.location(), "Body types are not of type void !!");
			}
		}
		SemAn.ofType.put(stmtWhile, new SemVoid());
		
		return null;
	}

	// stmt
	@Override
	public SemType visit(AstAssignStmt assignStmt, Mode mode) {
		assignStmt.src().accept(this, mode);
		assignStmt.dst().accept(this, mode);
		
		SemType assignSrc = SemAn.ofType.get(assignStmt.src());
		SemType assignDst = SemAn.ofType.get(assignStmt.dst());
		
		if((assignSrc instanceof SemInteger && assignDst instanceof SemInteger) 
		|| (assignSrc instanceof SemChar    && assignDst instanceof SemChar) 
		|| (assignSrc instanceof SemPointer && assignDst instanceof SemPointer)){
			SemAn.ofType.put(assignStmt, new SemVoid());
		}
		else{
			throw new Report.Error(assignStmt.location(), "Assign statement expresions are of incorrect types");
		}

		return null;
	}

	@Override
	public SemType visit(AstBinExpr binExpr, Mode mode) {
		binExpr.fstExpr().accept(this, mode);
		binExpr.sndExpr().accept(this, mode);

		SemType fstExpr = SemAn.ofType.get(binExpr.fstExpr());
		SemType sndExpr = SemAn.ofType.get(binExpr.sndExpr());
		
		if(binExpr.oper().toString().matches("ADD|SUB|MUL|DIV|MOD")){
			if(fstExpr instanceof SemInteger && sndExpr instanceof SemInteger){
				SemAn.ofType.put(binExpr, new SemInteger());
			}
			else{
				throw new Report.Error(binExpr.location(), "Binary operators +, -, *, / in % "
				+ "can only be assigned to ints !!");
			}
		}

		if(binExpr.oper().toString().matches("EQU|NEQ|LTH|GTH|LEQ|GEQ")){	
			// if problems arise put here only eq and neq and seperate the others

			if((fstExpr instanceof SemInteger && sndExpr instanceof SemInteger) 
			|| (fstExpr instanceof SemChar && sndExpr instanceof SemChar) 
			|| (fstExpr instanceof SemPointer && sndExpr instanceof SemPointer)){
				SemAn.ofType.put(binExpr, new SemInteger());
			}
			else {
				throw new Report.Error(binExpr.location(), "Binary operators == in != "
				+ "can only be assigned to ints, chars and pointers !!");
			}
		}

		
		return null;
	}



	// from the top baby
	@Override
	public SemType visit(AstArrExpr arrExpr, Mode mode) {
		arrExpr.idx().accept(this, mode);
		arrExpr.arr().accept(this, mode);

		SemType arrIdx = SemAn.ofType.get(arrExpr.idx()).actualType();	
		// i need this because you have problems with NameType
		SemType arrArr = SemAn.ofType.get(arrExpr.arr()).actualType();

		if(arrIdx instanceof SemArray){
			if(arrArr instanceof SemInteger){
				SemAn.ofType.put(arrExpr, ((SemArray) arrIdx).elemType());
			}
			else{
				throw new Report.Error(arrExpr.location(), "Indexes can only be "
				+ "be integers !!");
			}
		}
		else{
			throw new Report.Error(arrExpr.location(), "Indexes can only be "
			+ "acces on variables that are arrays !!");
		}
		
		return null;
	}

	@Override
	public SemType visit(AstCallExpr callExpr, Mode mode) {
		for(AstTree tree : callExpr.args()){
			tree.accept(this, mode);
		}

		AstTrees<AstParDecl> parameters = ((AstFunDecl)SemAn.declaredAt.get(callExpr)).pars();
		
		if(parameters.size() != callExpr.args().size()){
			throw new Report.Error(callExpr.location(), "Number of parameters given to the function is not correct !!");
		}

		for(int i = 0; i < parameters.size(); i++){
			SemType typeParam = SemAn.isType.get(parameters.get(i).type());
			SemType typeArgs = SemAn.ofType.get(callExpr.args().get(i));
			
			if(typeParam.getClass() != typeArgs.getClass()){
				throw new Report.Error(callExpr.location(), "Arguments given are not of the correct type !!");
			}
		}
	

		SemAn.ofType.put(callExpr, SemAn.isType.get(((AstFunDecl) SemAn.declaredAt.get(callExpr)).type()));
		// SemAn.ofType.put(callExpr, new SemInteger());

		return null;
	}

	@Override
	public SemType visit(AstCastExpr castExpr, Mode mode) {
		castExpr.expr().accept(this, mode);
		castExpr.type().accept(this, mode);

		SemType castExprE = SemAn.ofType.get(castExpr.expr());
		SemType typeExpr = SemAn.isType.get(castExpr.type());

		if(!(castExprE instanceof SemInteger || castExprE instanceof SemChar
		|| castExprE instanceof SemPointer)){
			throw new Report.Error(castExpr.location(), "Only ints, chars and pointer can be casted !!");
		}

		if(!(typeExpr instanceof SemInteger || typeExpr instanceof SemChar
		|| typeExpr instanceof SemPointer)) {
			throw new Report.Error(castExpr.location(), "You can cast only to ints, chars and pointers !!");
		}
		SemAn.ofType.put(castExpr, typeExpr);

		return null;
	}

	@Override
	public SemType visit(AstPfxExpr pfxExpr, Mode mode) {
		pfxExpr.expr().accept(this, mode);
		String pfxOper = pfxExpr.oper().toString();

		if(pfxOper.equals("ADD") || pfxOper.equals("SUB")){
			if(!(SemAn.ofType.get(pfxExpr.expr()) instanceof SemInteger)){
				throw new Report.Error(pfxExpr.location(), "Prefix + and - can only be given to int !!");
			}
			else {
				SemAn.ofType.put(pfxExpr, new SemInteger());
			}
		}

		if(pfxOper.equals("PTR")){
			SemAn.ofType.put(pfxExpr, new SemPointer(SemAn.ofType.get(pfxExpr.expr())));
		}

		if(pfxOper.equals("NEW")){
			if(SemAn.ofType.get(pfxExpr.expr()) instanceof SemInteger){
				SemAn.ofType.put(pfxExpr, new SemPointer(new SemVoid()));
			}
			else{
				throw new Report.Error(pfxExpr.location(), "Cannot make a pointer to an expresion which is not int !!");
			}
		}

		if(pfxOper.equals("DEL")){
			if(SemAn.ofType.get(pfxExpr.expr()) instanceof SemPointer){
				SemAn.ofType.put(pfxExpr, new SemVoid());
			}
			else{
				throw new Report.Error(pfxExpr.location(), "Cannot delete a pointer from an expresion which not a pointer !!");
			}
		}
		
		return null;
	}

	@Override
	public SemType visit(AstSfxExpr sfxExpr, Mode mode) {
		sfxExpr.expr().accept(this, mode);
		String sfxOper = sfxExpr.oper().toString();

		if(sfxOper.equals("PTR")){
			if(SemAn.ofType.get(sfxExpr.expr()) instanceof SemPointer){
				SemType baseType = ((SemPointer) SemAn.ofType.get(sfxExpr.expr())).baseType();
				SemAn.ofType.put(sfxExpr, baseType);
			}
			else {
				throw new Report.Error(sfxExpr.location(), "You can only dereference pointers !!");
			}
		}
		
		return null;
	}
	
	@Override
	public SemType visit(AstWhereExpr whereExpr, Mode mode) {
		whereExpr.decls().accept(this, mode);
		whereExpr.expr().accept(this, mode);
		SemAn.ofType.put(whereExpr, SemAn.ofType.get(whereExpr.expr()));

		return null;
	}

	// stmt
	@Override
	public SemType visit(AstExprStmt exprStmt, Mode mode) {
		exprStmt.expr().accept(this, mode);

		SemAn.ofType.put(exprStmt, SemAn.ofType.get(exprStmt.expr()));
		// SemType stmtsType =  SemAn.ofType.put(stmtExpr.stmts().get(stmtExpr.stmts().size() - 1));
		
		return null;
	}

	@Override
	public SemType visit(AstIfStmt ifStmt, Mode mode) {
		ifStmt.cond().accept(this, mode);

		for(AstTree tree : ifStmt.thenStmts()){
			tree.accept(this, mode);
		}
		for(AstTree tree : ifStmt.elseStmts()){
			tree.accept(this, mode);
		}
		SemType condition = SemAn.ofType.get(ifStmt.cond());

		if(!(condition instanceof SemInteger)){
			throw new Report.Error(ifStmt.location(), "Condition must be of integer type !!");
		}

		for(int i = 0; i < ifStmt.thenStmts().size(); i++){
			SemType thenStmts = SemAn.ofType.get(ifStmt.thenStmts().get(i));
			if(!(thenStmts instanceof SemVoid)){
				throw new Report.Error(ifStmt.location(), "Then stmts are not of type void !!");
			}
		}

		for(int i = 0; i < ifStmt.elseStmts().size(); i++){
			SemType elseStmts = SemAn.ofType.get(ifStmt.elseStmts().get(i));
			if(!(elseStmts instanceof SemVoid)){
				throw new Report.Error(ifStmt.location(), "Then stmts are not of type void !!");
			}
		}
		SemAn.ofType.put(ifStmt, new SemVoid());

		return null;
	}

	// type
	@Override
	public SemType visit(AstPtrType ptrType, Mode mode) {
		ptrType.baseType().accept(this, mode);
		SemAn.isType.put(ptrType, new SemPointer(SemAn.isType.get(ptrType.baseType())));
		
		return null;
	}
}
