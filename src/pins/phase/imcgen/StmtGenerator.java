package pins.phase.imcgen;

import java.util.*;

import pins.data.imc.code.expr.*;
import pins.data.ast.tree.stmt.*;
import pins.data.ast.visitor.*;
import pins.data.mem.*;
import pins.data.imc.code.stmt.*;

public class StmtGenerator implements AstVisitor<ImcStmt, Stack<MemFrame>> {

	// TODO
	// Stmts
	@Override
	public ImcStmt visit(AstExprStmt exprStmt, Stack<MemFrame> frames) {
		ImcExpr tempImc = exprStmt.expr().accept(new ExprGenerator(), frames);
		ImcStmt exprImc = new ImcESTMT(tempImc);

		ImcGen.stmtImc.put(exprStmt, exprImc);
		return exprImc;
	}

	@Override
	public ImcStmt visit(AstWhileStmt stmtWhile, Stack<MemFrame> frames) {
		Vector<ImcStmt> vectorWhile = new Vector();
		MemLabel l1 = new MemLabel();
		MemLabel l2 = new MemLabel();
		MemLabel l3 = new MemLabel();
		ImcExpr condImc = stmtWhile.cond().accept(new ExprGenerator(), frames);
		ImcStmt whileImc = new ImcCJUMP(condImc, l2, l3);

		vectorWhile.add(new ImcLABEL(l1));
		vectorWhile.add(whileImc);
		vectorWhile.add(new ImcLABEL(l2));
		// System.out.println("while gang " + condImc);

		for(AstStmt tree : stmtWhile.bodyStmts()){
			ImcStmt temp = tree.accept(this, frames);
			vectorWhile.add(temp);
		}
		vectorWhile.add(new ImcJUMP(l1));
		vectorWhile.add(new ImcLABEL(l3));

		ImcSTMTS whileImcG = new ImcSTMTS(vectorWhile);
		ImcGen.stmtImc.put(stmtWhile, whileImcG);
		
		return whileImcG;
	}


	@Override
	public ImcStmt visit(AstAssignStmt assignStmt, Stack<MemFrame> frames) {
		ExprGenerator exprGen = new ExprGenerator();

		ImcExpr srcImc = assignStmt.src().accept(exprGen, frames);
		ImcExpr dstImc = assignStmt.dst().accept(exprGen, frames);

		ImcStmt assignImc = new ImcMOVE(dstImc, srcImc);

		ImcGen.stmtImc.put(assignStmt, assignImc);
		return assignImc;
	}

	@Override
	public ImcStmt visit(AstIfStmt ifStmt, Stack<MemFrame> frames) {
		Vector<ImcStmt> vectorIf = new Vector();
		MemLabel startLabel = new MemLabel();
		MemLabel elseLabel = new MemLabel();
		MemLabel endLabel = new MemLabel();

		ImcExpr condImc = ifStmt.cond().accept(new ExprGenerator(), frames);
		ImcStmt ifCond = new ImcCJUMP(condImc, startLabel, elseLabel);
		vectorIf.add(ifCond);

		vectorIf.add(new ImcLABEL(startLabel));
		for(AstStmt tree : ifStmt.thenStmts()){
			ImcStmt temp = tree.accept(this, frames);
			vectorIf.add(temp);
		}
		vectorIf.add(new ImcJUMP(endLabel));

		vectorIf.add(new ImcLABEL(elseLabel));
		for(AstStmt tree : ifStmt.elseStmts()){
			ImcStmt temp = tree.accept(this, frames);
			vectorIf.add(temp);
		}
		vectorIf.add(new ImcLABEL(endLabel));

		ImcSTMTS ifImc = new ImcSTMTS(vectorIf);
		ImcGen.stmtImc.put(ifStmt, ifImc);
		return ifImc;
	}
}
