package pins.phase.imcgen;

import java.util.*;

import pins.common.report.*;
import pins.data.ast.tree.*;
import pins.data.ast.tree.decl.*;
import pins.data.ast.tree.expr.*;
import pins.data.ast.tree.stmt.*;
import pins.data.ast.tree.type.*;
import pins.data.ast.visitor.*;
import pins.data.semtype.*;
import pins.data.mem.*;
import pins.data.imc.code.expr.*;
import pins.data.imc.code.stmt.*;
import pins.phase.seman.*;
import pins.phase.memory.*;

public class ExprGenerator implements AstVisitor<ImcExpr, Stack<MemFrame>> {
	// vsako drevo vrnes in vstavis v atribut - en za stmt
	// ImcGen sm vstavljas drevesa

	// TODO
	@Override
	public ImcExpr visit(AstTrees<? extends AstTree> trees, Stack<MemFrame> frames) {
		for (AstTree tree : trees){
			if (tree != null){
				tree.accept(this, frames);
			}
		}
		return null;
	}

	// Expr 
	public ImcExpr visit(AstAtomExpr atomExpr, Stack<MemFrame> frames) {
		ImcExpr atomImc = null;

		if(atomExpr.type() == AstAtomExpr.Type.CHAR){
			String value = atomExpr.value();
			char current_c = (value.charAt(1) == '\\') ? value.charAt(2) : value.charAt(1);
			// if special char get the second value, if not get first
			atomImc = new ImcCONST(current_c);
		}
		else if(atomExpr.type() == AstAtomExpr.Type.INT){
			atomImc = new ImcCONST(Long.parseLong(atomExpr.value()));
		}		
		else{
			//TODO maybe add more checking - this now handles ints, voids ...
			// System.err.println("Not of type char or int");	
			atomImc = new ImcCONST(0L);
		}
		ImcGen.exprImc.put(atomExpr, atomImc);

		return atomImc;
	}

	@Override
    public ImcExpr visit(AstNameExpr nameExpr, Stack<MemFrame> frames) {
		AstDecl typeOfDecl = SemAn.declaredAt.get(nameExpr);
		ImcExpr memIMC = null;
		MemAccess memAcc = null;

		if(typeOfDecl instanceof AstParDecl){
			memAcc = Memory.accesses.get((AstParDecl)typeOfDecl);
			memIMC = new ImcMEM(new ImcBINOP(ImcBINOP.Oper.ADD, new ImcTEMP(frames.peek().FP), new ImcCONST(((MemRelAccess)memAcc).offset)));
		}
		else if(typeOfDecl instanceof AstVarDecl){
			memAcc = Memory.accesses.get((AstVarDecl)typeOfDecl);

			if(memAcc instanceof MemAbsAccess){
				ImcNAME nameImc = new ImcNAME(((MemAbsAccess)memAcc).label);
				memIMC = new ImcMEM(nameImc);
			}
			else if(memAcc instanceof MemRelAccess){
				memIMC = new ImcMEM(new ImcBINOP(ImcBINOP.Oper.ADD, new ImcTEMP(frames.peek().FP), new ImcCONST(((MemRelAccess)memAcc).offset)));
			}
			else{
				throw new Report.Error(nameExpr.location(), "Unknown memory access !");
			}
		}
		else{
			throw new Report.Error(nameExpr.location(), "Unknown access to nameExpresion !");
		}
		ImcGen.exprImc.put(nameExpr, memIMC);
        return memIMC;
	}

	@Override
	public ImcExpr visit(AstBinExpr binExpr, Stack<MemFrame> frames) {
		ImcExpr fstImc = binExpr.fstExpr().accept(this, frames);
		ImcExpr sndImc = binExpr.sndExpr().accept(this, frames);
		ImcBINOP.Oper operand = null;

		switch (binExpr.oper().toString()) {
			case "EQU":
				operand = ImcBINOP.Oper.EQU;
				break;
			case "NEQ":
				operand = ImcBINOP.Oper.NEQ;
				break;
			case "LTH":
				operand = ImcBINOP.Oper.LTH;
				break;
			case "GTH":
				operand = ImcBINOP.Oper.GTH;
				break;
			case "LEQ":
				operand = ImcBINOP.Oper.LEQ;
				break;
			case "GEQ":
				operand = ImcBINOP.Oper.GEQ;
				break;
			case "ADD":
				operand = ImcBINOP.Oper.ADD;
				break;
			case "SUB":
				operand = ImcBINOP.Oper.SUB;
				break;
			case "MUL":
				operand = ImcBINOP.Oper.MUL;
				break;
			case "DIV":
				operand = ImcBINOP.Oper.DIV;
				break;
			case "MOD":
				operand = ImcBINOP.Oper.MOD;
				break;

			default:
				throw new Report.Error(binExpr.location(), "Unknown operator !");
		}
		// System.out.println("n1 " + fstImc + " n2 " + sndImc + " Oper " + operand);
		ImcExpr binImc = new ImcBINOP(operand, fstImc, sndImc);
		ImcGen.exprImc.put(binExpr, binImc);

		return binImc;
	}

	@Override
	public ImcExpr visit(AstCallExpr callExpr, Stack<MemFrame> frames) {
		Vector<ImcExpr> args = new Vector();
		Vector<Long> offsets = new Vector();

		args.add(new ImcCONST(0));
		offsets.add(0L);

		AstTrees<AstExpr> trees = callExpr.args();
		for(int i = 0; i < trees.size(); i++){
			ImcExpr temp = trees.get(i).accept(this, frames);
			args.add(temp);		
			offsets.add(8L * (i + 1));				
			// change this to just 8 if any problems
		}

		AstFunDecl funDecl = (AstFunDecl) SemAn.declaredAt.get(callExpr);
		MemLabel current_label = Memory.frames.get(funDecl).label;

		ImcExpr callImc = new ImcCALL(current_label, offsets, args);
		ImcGen.exprImc.put(callExpr, callImc);
		
		return callImc;
	}

	@Override
	public ImcExpr visit(AstArrExpr arrExpr, Stack<MemFrame> frames) {
		ImcExpr id_arr = arrExpr.idx().accept(this, frames);
		ImcExpr arr = arrExpr.arr().accept(this, frames);
		ImcExpr indexAdr = new ImcBINOP(ImcBINOP.Oper.MUL, new ImcCONST(8L), arr);
		ImcExpr arrImc = new ImcMEM(new ImcBINOP(ImcBINOP.Oper.ADD, (((ImcMEM)id_arr).addr), indexAdr)); 

		// System.out.println("adr "+ (((ImcMEM)id_arr).addr) + " arrExpr ... id " + id_arr + " arr " + indexAdr);
		ImcGen.exprImc.put(arrExpr, arrImc);
		return arrImc;
	}

	@Override
	public ImcExpr visit(AstCastExpr castExpr, Stack<MemFrame> frames) {
		ImcExpr castImc = castExpr.expr().accept(this, frames);
		// System.out.println("cast " + castImc);
		// castExpr.type().accept(this, frames);

		ImcGen.exprImc.put(castExpr, castImc);
		
		return castImc;
	}

	@Override
	public ImcExpr visit(AstPfxExpr pfxExpr, Stack<MemFrame> frames) {
		ImcExpr pfx = pfxExpr.expr().accept(this, frames);
		
		if(pfxExpr.oper().toString().equals("SUB")){
			ImcExpr pfxImc = new ImcUNOP(ImcUNOP.Oper.NEG, pfx);

			ImcGen.exprImc.put(pfxExpr, pfxImc);
			return pfxImc;
		}

		return null;
	}

	@Override
	public ImcExpr visit(AstSfxExpr sfxExpr, Stack<MemFrame> frames) {
		ImcMEM sfxImc = new ImcMEM(sfxExpr.expr().accept(this, frames)); 
		ImcGen.exprImc.put(sfxExpr, sfxImc);
		return sfxImc;

    }
	
	@Override
	public ImcExpr visit(AstWhereExpr whereExpr, Stack<MemFrame> frames) {
		ImcExpr declsImc = whereExpr.decls().accept(this, frames);
		ImcExpr exprImc = whereExpr.expr().accept(this, frames);
		// System.out.println("whereExpr e " + exprImc + " decls " + declsImc);

		ImcGen.exprImc.put(whereExpr, exprImc);
		
		return exprImc;
	}

	@Override
	public ImcExpr visit(AstStmtExpr stmtExpr, Stack<MemFrame> frames) {
		StmtGenerator stmtGen = new StmtGenerator();
		Vector<ImcStmt> vectorImcStmts = new Vector();

		for(AstStmt tree : stmtExpr.stmts()){
			ImcStmt current_stmt = tree.accept(stmtGen, frames);
			vectorImcStmts.add(current_stmt);
			// System.out.println("stmtExpr " + current_stmt);
		}

		ImcSTMTS imcStmts = new ImcSTMTS(vectorImcStmts);
		ImcSEXPR imcSexpr = null;

		ImcStmt lastElement = vectorImcStmts.get(vectorImcStmts.size() - 1);
		if(lastElement instanceof ImcESTMT){
			imcSexpr = new ImcSEXPR(imcStmts, ((ImcESTMT)lastElement).expr);
		}
		else{
			imcSexpr = new ImcSEXPR(imcStmts, new ImcCONST(0));
		}

		ImcGen.exprImc.put(stmtExpr, imcSexpr);
		
		return imcSexpr;
	}
	
	// Type
	/*@Override
	public ImcExpr visit(AstAtomType atomType, Stack<MemFrame> frames) {
		return null;
	}


	@Override
    public ImcExpr visit(AstPtrType ptrType, Stack<MemFrame> frames) {
		ptrType.baseType().accept(this, frames);

        return null;
	}

	@Override
    public ImcExpr visit(AstTypeDecl typeDecl, Stack<MemFrame> frames) {
		typeDecl.type().accept(this, frames);

        return null;
	}

	@Override
	public ImcExpr visit(AstArrType arrType, Stack<MemFrame> frames) {
		arrType.elemType().accept(this, frames);
		arrType.numElems().accept(this, frames);

		return null;
	}

	@Override
    public ImcExpr visit(AstNameType nameType, Stack<MemFrame> frames) {
        // AstTypeDecl typeDecl = ((AstTypeDecl)SemAn.declaredAt.get(nameType));
        return null;
	} */

	@Override
    public ImcExpr visit(AstFunDecl funDecl, Stack<MemFrame> frames) {		
		/*for (AstTree tree : funDecl.pars()){
			tree.accept(this, frames);
		}
		funDecl.type().accept(this, frames);
		*/
		frames.push(Memory.frames.get(funDecl));
		funDecl.expr().accept(this, frames);
		frames.pop();

        return null;
	}
	

	// parDecl
	@Override
    public ImcExpr visit(AstParDecl parDecl, Stack<MemFrame> frames) {
		// parDecl.type().accept(this, frames);
        return null;
	}
	@Override
    public ImcExpr visit(AstVarDecl varDecl, Stack<MemFrame> frames) {
        return null;
	}
}
