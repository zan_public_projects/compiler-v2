// Generated from synan/PinsParser.g4 by ANTLR 4.8


	package pins.phase.synan;
	
	import java.util.*;
	
	import pins.common.report.*;
	import pins.phase.lexan.*;
	import pins.data.ast.tree.*;
	import pins.data.ast.tree.decl.*;	
	import pins.data.ast.tree.expr.*;
	import pins.data.ast.tree.stmt.*;
	import pins.data.ast.tree.type.*;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class PinsParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		LP=1, RP=2, LBR=3, RBR=4, LB=5, RB=6, DOT=7, COLON=8, SEMICOLON=9, COMA=10, 
		AND=11, OR=12, NEGATE=13, EQUAL=14, NOTEQUAL=15, LT=16, GT=17, LE=18, 
		GE=19, MUL=20, DIVIDE=21, MOD=22, PLUS=23, MINUS=24, POW=25, ASSIGN=26, 
		CHAR=27, DEL=28, DO=29, ELSE=30, END=31, FUN=32, IF=33, INT=34, NEW=35, 
		THEN=36, TYP=37, VAR=38, VOID=39, WHERE=40, WHILE=41, NONE=42, NIL=43, 
		WHITESPACE=44, WHITESPACET=45, ID=46, INTCONST=47, CHARCONST=48, OPENCOMMENT=49, 
		CLOSEDCOMMENT=50, ONELINECOMMENT=51, ERROR=52;
	public static final int
		RULE_source = 0, RULE_decl = 1, RULE_type = 2, RULE_expr = 3, RULE_pre = 4, 
		RULE_dis = 5, RULE_disP = 6, RULE_con = 7, RULE_conP = 8, RULE_rel = 9, 
		RULE_relP = 10, RULE_add = 11, RULE_addP = 12, RULE_mul = 13, RULE_mulP = 14, 
		RULE_prex = 15, RULE_pfx = 16, RULE_pfxP = 17, RULE_endExpr = 18, RULE_stmt = 19;
	private static String[] makeRuleNames() {
		return new String[] {
			"source", "decl", "type", "expr", "pre", "dis", "disP", "con", "conP", 
			"rel", "relP", "add", "addP", "mul", "mulP", "prex", "pfx", "pfxP", "endExpr", 
			"stmt"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'('", "')'", "'{'", "'}'", "'['", "']'", "'.'", "':'", "';'", 
			"','", "'&'", "'|'", "'!'", "'=='", "'!='", "'<'", "'>'", "'<='", "'>='", 
			"'*'", "'/'", "'%'", "'+'", "'-'", "'^'", "'='", "'char'", "'del'", "'do'", 
			"'else'", "'end'", "'fun'", "'if'", "'int'", "'new'", "'then'", "'typ'", 
			"'var'", "'void'", "'where'", "'while'", "'none'", "'nil'", null, null, 
			null, null, null, "'#{'", "'}#'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "LP", "RP", "LBR", "RBR", "LB", "RB", "DOT", "COLON", "SEMICOLON", 
			"COMA", "AND", "OR", "NEGATE", "EQUAL", "NOTEQUAL", "LT", "GT", "LE", 
			"GE", "MUL", "DIVIDE", "MOD", "PLUS", "MINUS", "POW", "ASSIGN", "CHAR", 
			"DEL", "DO", "ELSE", "END", "FUN", "IF", "INT", "NEW", "THEN", "TYP", 
			"VAR", "VOID", "WHERE", "WHILE", "NONE", "NIL", "WHITESPACE", "WHITESPACET", 
			"ID", "INTCONST", "CHARCONST", "OPENCOMMENT", "CLOSEDCOMMENT", "ONELINECOMMENT", 
			"ERROR"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "PinsParser.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }


		public int getLine(Token token){
			return token.getLine();
		}

		public int getStartCharPos(Token token){
			return token.getCharPositionInLine();
		}

		public int getEndCharPos(Token token){
			int temp = token.getCharPositionInLine() + token.getText().length() - 1;
			return temp;
		}

	public PinsParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class SourceContext extends ParserRuleContext {
		public AstTrees<AstDecl> ast;
		public DeclContext decl;
		public List<DeclContext> decl() {
			return getRuleContexts(DeclContext.class);
		}
		public DeclContext decl(int i) {
			return getRuleContext(DeclContext.class,i);
		}
		public SourceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_source; }
	}

	public final SourceContext source() throws RecognitionException {
		SourceContext _localctx = new SourceContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_source);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			 Vector decl = new Vector(); 
			setState(44); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(41);
				((SourceContext)_localctx).decl = decl();
				 decl.add(((SourceContext)_localctx).decl.ast); 
				}
				}
				setState(46); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << FUN) | (1L << TYP) | (1L << VAR))) != 0) );
			 ((SourceContext)_localctx).ast =  new AstTrees(decl); 
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclContext extends ParserRuleContext {
		public AstDecl ast;
		public Token TYP;
		public Token ID;
		public TypeContext type;
		public Token SEMICOLON;
		public Token VAR;
		public Token FUN;
		public Token i_main;
		public Token i1;
		public TypeContext ty_1;
		public Token COMA;
		public Token i2;
		public TypeContext ty_2;
		public TypeContext t1;
		public ExprContext ex1;
		public TerminalNode TYP() { return getToken(PinsParser.TYP, 0); }
		public List<TerminalNode> ID() { return getTokens(PinsParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(PinsParser.ID, i);
		}
		public TerminalNode ASSIGN() { return getToken(PinsParser.ASSIGN, 0); }
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public TerminalNode SEMICOLON() { return getToken(PinsParser.SEMICOLON, 0); }
		public TerminalNode VAR() { return getToken(PinsParser.VAR, 0); }
		public List<TerminalNode> COLON() { return getTokens(PinsParser.COLON); }
		public TerminalNode COLON(int i) {
			return getToken(PinsParser.COLON, i);
		}
		public TerminalNode FUN() { return getToken(PinsParser.FUN, 0); }
		public TerminalNode LP() { return getToken(PinsParser.LP, 0); }
		public TerminalNode RP() { return getToken(PinsParser.RP, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public List<TerminalNode> COMA() { return getTokens(PinsParser.COMA); }
		public TerminalNode COMA(int i) {
			return getToken(PinsParser.COMA, i);
		}
		public DeclContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_decl; }
	}

	public final DeclContext decl() throws RecognitionException {
		DeclContext _localctx = new DeclContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_decl);
		int _la;
		try {
			setState(93);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TYP:
				enterOuterAlt(_localctx, 1);
				{
				setState(50);
				((DeclContext)_localctx).TYP = match(TYP);
				setState(51);
				((DeclContext)_localctx).ID = match(ID);
				setState(52);
				match(ASSIGN);
				setState(53);
				((DeclContext)_localctx).type = type();
				setState(54);
				((DeclContext)_localctx).SEMICOLON = match(SEMICOLON);
				 ((DeclContext)_localctx).ast =  new AstTypeDecl(new Location((LexAn.PrevToken)((DeclContext)_localctx).TYP, (LexAn.PrevToken)((DeclContext)_localctx).SEMICOLON), ((DeclContext)_localctx).ID.getText(), ((DeclContext)_localctx).type.ast); 
				}
				break;
			case VAR:
				enterOuterAlt(_localctx, 2);
				{
				setState(57);
				((DeclContext)_localctx).VAR = match(VAR);
				setState(58);
				((DeclContext)_localctx).ID = match(ID);
				setState(59);
				match(COLON);
				setState(60);
				((DeclContext)_localctx).type = type();
				setState(61);
				((DeclContext)_localctx).SEMICOLON = match(SEMICOLON);
				 ((DeclContext)_localctx).ast =  new AstVarDecl(new Location((LexAn.PrevToken)((DeclContext)_localctx).VAR, (LexAn.PrevToken)((DeclContext)_localctx).SEMICOLON), ((DeclContext)_localctx).ID.getText(), ((DeclContext)_localctx).type.ast); 
				}
				break;
			case FUN:
				enterOuterAlt(_localctx, 3);
				{
				 Vector vector = new Vector(); 
				setState(65);
				((DeclContext)_localctx).FUN = match(FUN);
				setState(66);
				((DeclContext)_localctx).i_main = match(ID);
				setState(67);
				match(LP);
				setState(83);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==ID) {
					{
					setState(68);
					((DeclContext)_localctx).i1 = match(ID);
					setState(69);
					match(COLON);
					setState(70);
					((DeclContext)_localctx).ty_1 = type();
					 vector.add(new AstParDecl(new Location((LexAn.PrevToken)((DeclContext)_localctx).i1, ((DeclContext)_localctx).ty_1.ast.location()), ((DeclContext)_localctx).i1.getText(), ((DeclContext)_localctx).ty_1.ast));
					setState(80);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMA) {
						{
						{
						setState(72);
						((DeclContext)_localctx).COMA = match(COMA);
						setState(73);
						((DeclContext)_localctx).i2 = match(ID);
						setState(74);
						match(COLON);
						setState(75);
						((DeclContext)_localctx).ty_2 = type();
						 vector.add(new AstParDecl(new Location((LexAn.PrevToken)((DeclContext)_localctx).COMA, ((DeclContext)_localctx).ty_2.ast.location()), ((DeclContext)_localctx).i2.getText(), ((DeclContext)_localctx).ty_2.ast)); 
						}
						}
						setState(82);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(85);
				match(RP);
				setState(86);
				match(COLON);
				setState(87);
				((DeclContext)_localctx).t1 = type();
				setState(88);
				match(ASSIGN);
				setState(89);
				((DeclContext)_localctx).ex1 = expr();
				setState(90);
				((DeclContext)_localctx).SEMICOLON = match(SEMICOLON);

						AstTrees<AstParDecl> tree = new AstTrees(vector);
						((DeclContext)_localctx).ast =  new AstFunDecl(new Location((LexAn.PrevToken)((DeclContext)_localctx).FUN, (LexAn.PrevToken)((DeclContext)_localctx).SEMICOLON), 
						((DeclContext)_localctx).i_main.getText(), tree, ((DeclContext)_localctx).t1.ast, ((DeclContext)_localctx).ex1.ast);
					
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public AstType ast;
		public Token VOID;
		public Token CHAR;
		public Token INT;
		public Token ID;
		public Token LB;
		public ExprContext expr;
		public Token RB;
		public TypeContext type;
		public Token POW;
		public TypeContext ty_1;
		public Token LP;
		public Token RP;
		public TerminalNode VOID() { return getToken(PinsParser.VOID, 0); }
		public TerminalNode CHAR() { return getToken(PinsParser.CHAR, 0); }
		public TerminalNode INT() { return getToken(PinsParser.INT, 0); }
		public TerminalNode ID() { return getToken(PinsParser.ID, 0); }
		public TerminalNode LB() { return getToken(PinsParser.LB, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode RB() { return getToken(PinsParser.RB, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode POW() { return getToken(PinsParser.POW, 0); }
		public TerminalNode LP() { return getToken(PinsParser.LP, 0); }
		public TerminalNode RP() { return getToken(PinsParser.RP, 0); }
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_type);
		try {
			setState(118);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case VOID:
				enterOuterAlt(_localctx, 1);
				{
				setState(95);
				((TypeContext)_localctx).VOID = match(VOID);
				((TypeContext)_localctx).ast =  new AstAtomType(new Location((LexAn.PrevToken)((TypeContext)_localctx).VOID), AstAtomType.Type.VOID);
				}
				break;
			case CHAR:
				enterOuterAlt(_localctx, 2);
				{
				setState(97);
				((TypeContext)_localctx).CHAR = match(CHAR);
				((TypeContext)_localctx).ast =  new AstAtomType(new Location((LexAn.PrevToken)((TypeContext)_localctx).CHAR), AstAtomType.Type.CHAR);
				}
				break;
			case INT:
				enterOuterAlt(_localctx, 3);
				{
				setState(99);
				((TypeContext)_localctx).INT = match(INT);
				((TypeContext)_localctx).ast =  new AstAtomType(new Location((LexAn.PrevToken)((TypeContext)_localctx).INT) , AstAtomType.Type.INT);
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 4);
				{
				setState(101);
				((TypeContext)_localctx).ID = match(ID);
				((TypeContext)_localctx).ast =  new AstNameType(new Location((LexAn.PrevToken)((TypeContext)_localctx).ID), ((TypeContext)_localctx).ID.getText());
				}
				break;
			case LB:
				enterOuterAlt(_localctx, 5);
				{
				setState(103);
				((TypeContext)_localctx).LB = match(LB);
				setState(104);
				((TypeContext)_localctx).expr = expr();
				setState(105);
				((TypeContext)_localctx).RB = match(RB);
				setState(106);
				((TypeContext)_localctx).type = type();
					((TypeContext)_localctx).ast =  new AstArrType(new Location((LexAn.PrevToken)((TypeContext)_localctx).LB, (LexAn.PrevToken)((TypeContext)_localctx).RB), ((TypeContext)_localctx).type.ast, ((TypeContext)_localctx).expr.ast);
				}
				break;
			case POW:
				enterOuterAlt(_localctx, 6);
				{
				setState(109);
				((TypeContext)_localctx).POW = match(POW);
				setState(110);
				((TypeContext)_localctx).ty_1 = type();
				((TypeContext)_localctx).ast =  new AstPtrType(new Location((LexAn.PrevToken)((TypeContext)_localctx).POW, ((TypeContext)_localctx).ty_1.ast.location()), ((TypeContext)_localctx).ty_1.ast);
				}
				break;
			case LP:
				enterOuterAlt(_localctx, 7);
				{
				setState(113);
				((TypeContext)_localctx).LP = match(LP);
				setState(114);
				((TypeContext)_localctx).ty_1 = type();
				setState(115);
				((TypeContext)_localctx).RP = match(RP);
					((TypeContext)_localctx).ty_1.ast.relocate(new Location((LexAn.PrevToken)((TypeContext)_localctx).LP, (LexAn.PrevToken)((TypeContext)_localctx).RP));
						((TypeContext)_localctx).ast =  ((TypeContext)_localctx).ty_1.ast;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public AstExpr ast;
		public PreContext t1;
		public PreContext pre;
		public ExprContext end;
		public DisContext dis;
		public Token LBR;
		public StmtContext nes1;
		public Token RBR;
		public PreContext pre() {
			return getRuleContext(PreContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public DisContext dis() {
			return getRuleContext(DisContext.class,0);
		}
		public TerminalNode LBR() { return getToken(PinsParser.LBR, 0); }
		public TerminalNode RBR() { return getToken(PinsParser.RBR, 0); }
		public List<StmtContext> stmt() {
			return getRuleContexts(StmtContext.class);
		}
		public StmtContext stmt(int i) {
			return getRuleContext(StmtContext.class,i);
		}
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
	}

	public final ExprContext expr() throws RecognitionException {
		ExprContext _localctx = new ExprContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_expr);
		int _la;
		try {
			setState(139);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DEL:
			case NEW:
				enterOuterAlt(_localctx, 1);
				{
				setState(120);
				((ExprContext)_localctx).t1 = ((ExprContext)_localctx).pre = pre();
				setState(121);
				((ExprContext)_localctx).end = expr();
				((ExprContext)_localctx).ast =  new AstPfxExpr(new Location(((ExprContext)_localctx).t1.loc , ((ExprContext)_localctx).end.ast.location()), ((ExprContext)_localctx).pre.opr, ((ExprContext)_localctx).end.ast);
				}
				break;
			case LP:
			case NEGATE:
			case PLUS:
			case MINUS:
			case POW:
			case NONE:
			case NIL:
			case ID:
			case INTCONST:
			case CHARCONST:
				enterOuterAlt(_localctx, 2);
				{
				setState(124);
				((ExprContext)_localctx).dis = dis();
				((ExprContext)_localctx).ast =  ((ExprContext)_localctx).dis.ast;
				}
				break;
			case LBR:
				enterOuterAlt(_localctx, 3);
				{
				 Vector vector = new Vector(); 
				setState(128);
				((ExprContext)_localctx).LBR = match(LBR);
				setState(132); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(129);
					((ExprContext)_localctx).nes1 = stmt();
					 vector.add(((ExprContext)_localctx).nes1.ast); 
					}
					}
					setState(134); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LP) | (1L << LBR) | (1L << NEGATE) | (1L << PLUS) | (1L << MINUS) | (1L << POW) | (1L << DEL) | (1L << IF) | (1L << NEW) | (1L << WHILE) | (1L << NONE) | (1L << NIL) | (1L << ID) | (1L << INTCONST) | (1L << CHARCONST))) != 0) );
				setState(136);
				((ExprContext)_localctx).RBR = match(RBR);
				 AstTrees<AstStmt> tree = new AstTrees(vector);
						((ExprContext)_localctx).ast =  new AstStmtExpr(new Location((LexAn.PrevToken)((ExprContext)_localctx).LBR, (LexAn.PrevToken)((ExprContext)_localctx).RBR), tree);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PreContext extends ParserRuleContext {
		public AstPfxExpr.Oper opr;
		public LexAn.PrevToken loc;
		public Token NEW;
		public Token DEL;
		public TerminalNode NEW() { return getToken(PinsParser.NEW, 0); }
		public TerminalNode DEL() { return getToken(PinsParser.DEL, 0); }
		public PreContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pre; }
	}

	public final PreContext pre() throws RecognitionException {
		PreContext _localctx = new PreContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_pre);
		try {
			setState(145);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NEW:
				enterOuterAlt(_localctx, 1);
				{
				setState(141);
				((PreContext)_localctx).NEW = match(NEW);
				((PreContext)_localctx).opr =  AstPfxExpr.Oper.NEW; ((PreContext)_localctx).loc =  (LexAn.PrevToken)((PreContext)_localctx).NEW;
				}
				break;
			case DEL:
				enterOuterAlt(_localctx, 2);
				{
				setState(143);
				((PreContext)_localctx).DEL = match(DEL);
				((PreContext)_localctx).opr =  AstPfxExpr.Oper.DEL; ((PreContext)_localctx).loc =  (LexAn.PrevToken)((PreContext)_localctx).DEL;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DisContext extends ParserRuleContext {
		public AstExpr ast;
		public ConContext con;
		public DisPContext disP;
		public ConContext con() {
			return getRuleContext(ConContext.class,0);
		}
		public DisPContext disP() {
			return getRuleContext(DisPContext.class,0);
		}
		public DisContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dis; }
	}

	public final DisContext dis() throws RecognitionException {
		DisContext _localctx = new DisContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_dis);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(147);
			((DisContext)_localctx).con = con();
			setState(148);
			((DisContext)_localctx).disP = disP(((DisContext)_localctx).con.ast);
			((DisContext)_localctx).ast =  ((DisContext)_localctx).disP.ast;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DisPContext extends ParserRuleContext {
		public AstExpr missing_ast;
		public AstExpr ast;
		public ConContext con;
		public DisPContext disP;
		public TerminalNode OR() { return getToken(PinsParser.OR, 0); }
		public ConContext con() {
			return getRuleContext(ConContext.class,0);
		}
		public DisPContext disP() {
			return getRuleContext(DisPContext.class,0);
		}
		public DisPContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public DisPContext(ParserRuleContext parent, int invokingState, AstExpr missing_ast) {
			super(parent, invokingState);
			this.missing_ast = missing_ast;
		}
		@Override public int getRuleIndex() { return RULE_disP; }
	}

	public final DisPContext disP(AstExpr missing_ast) throws RecognitionException {
		DisPContext _localctx = new DisPContext(_ctx, getState(), missing_ast);
		enterRule(_localctx, 12, RULE_disP);
		try {
			setState(157);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case OR:
				enterOuterAlt(_localctx, 1);
				{
				setState(151);
				match(OR);
				setState(152);
				((DisPContext)_localctx).con = con();
				setState(153);
				((DisPContext)_localctx).disP = disP(new AstBinExpr(new Location(_localctx.missing_ast.location(), ((DisPContext)_localctx).con.ast.location()), 
								  AstBinExpr.Oper.OR, _localctx.missing_ast, ((DisPContext)_localctx).con.ast));
				((DisPContext)_localctx).ast =  ((DisPContext)_localctx).disP.ast;
				}
				break;
			case RP:
			case RB:
			case COLON:
			case SEMICOLON:
			case COMA:
			case ASSIGN:
			case DO:
			case THEN:
			case WHERE:
				enterOuterAlt(_localctx, 2);
				{
				((DisPContext)_localctx).ast =  _localctx.missing_ast;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConContext extends ParserRuleContext {
		public AstExpr ast;
		public RelContext rel;
		public ConPContext conP;
		public RelContext rel() {
			return getRuleContext(RelContext.class,0);
		}
		public ConPContext conP() {
			return getRuleContext(ConPContext.class,0);
		}
		public ConContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_con; }
	}

	public final ConContext con() throws RecognitionException {
		ConContext _localctx = new ConContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_con);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(159);
			((ConContext)_localctx).rel = rel();
			setState(160);
			((ConContext)_localctx).conP = conP(((ConContext)_localctx).rel.ast);
			((ConContext)_localctx).ast =  ((ConContext)_localctx).conP.ast;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConPContext extends ParserRuleContext {
		public AstExpr missing_ast;
		public AstExpr ast;
		public RelContext rel;
		public ConPContext conP;
		public TerminalNode AND() { return getToken(PinsParser.AND, 0); }
		public RelContext rel() {
			return getRuleContext(RelContext.class,0);
		}
		public ConPContext conP() {
			return getRuleContext(ConPContext.class,0);
		}
		public ConPContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ConPContext(ParserRuleContext parent, int invokingState, AstExpr missing_ast) {
			super(parent, invokingState);
			this.missing_ast = missing_ast;
		}
		@Override public int getRuleIndex() { return RULE_conP; }
	}

	public final ConPContext conP(AstExpr missing_ast) throws RecognitionException {
		ConPContext _localctx = new ConPContext(_ctx, getState(), missing_ast);
		enterRule(_localctx, 16, RULE_conP);
		try {
			setState(169);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case AND:
				enterOuterAlt(_localctx, 1);
				{
				setState(163);
				match(AND);
				setState(164);
				((ConPContext)_localctx).rel = rel();
				setState(165);
				((ConPContext)_localctx).conP = conP(new AstBinExpr(new Location(_localctx.missing_ast.location(), ((ConPContext)_localctx).rel.ast.location()), 
								   AstBinExpr.Oper.AND, _localctx.missing_ast, ((ConPContext)_localctx).rel.ast));
				((ConPContext)_localctx).ast =  ((ConPContext)_localctx).conP.ast;
				}
				break;
			case RP:
			case RB:
			case COLON:
			case SEMICOLON:
			case COMA:
			case OR:
			case ASSIGN:
			case DO:
			case THEN:
			case WHERE:
				enterOuterAlt(_localctx, 2);
				{
				((ConPContext)_localctx).ast =  _localctx.missing_ast;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RelContext extends ParserRuleContext {
		public AstExpr ast;
		public AddContext add;
		public RelPContext relP;
		public AddContext add() {
			return getRuleContext(AddContext.class,0);
		}
		public RelPContext relP() {
			return getRuleContext(RelPContext.class,0);
		}
		public RelContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rel; }
	}

	public final RelContext rel() throws RecognitionException {
		RelContext _localctx = new RelContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_rel);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(171);
			((RelContext)_localctx).add = add();
			setState(172);
			((RelContext)_localctx).relP = relP(((RelContext)_localctx).add.ast);
			((RelContext)_localctx).ast =  ((RelContext)_localctx).relP.ast;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RelPContext extends ParserRuleContext {
		public AstExpr missing_ast;
		public AstExpr ast;
		public AddContext add;
		public RelPContext relP;
		public TerminalNode EQUAL() { return getToken(PinsParser.EQUAL, 0); }
		public AddContext add() {
			return getRuleContext(AddContext.class,0);
		}
		public RelPContext relP() {
			return getRuleContext(RelPContext.class,0);
		}
		public TerminalNode NOTEQUAL() { return getToken(PinsParser.NOTEQUAL, 0); }
		public TerminalNode LT() { return getToken(PinsParser.LT, 0); }
		public TerminalNode GT() { return getToken(PinsParser.GT, 0); }
		public TerminalNode LE() { return getToken(PinsParser.LE, 0); }
		public TerminalNode GE() { return getToken(PinsParser.GE, 0); }
		public RelPContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public RelPContext(ParserRuleContext parent, int invokingState, AstExpr missing_ast) {
			super(parent, invokingState);
			this.missing_ast = missing_ast;
		}
		@Override public int getRuleIndex() { return RULE_relP; }
	}

	public final RelPContext relP(AstExpr missing_ast) throws RecognitionException {
		RelPContext _localctx = new RelPContext(_ctx, getState(), missing_ast);
		enterRule(_localctx, 20, RULE_relP);
		try {
			setState(206);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case EQUAL:
				enterOuterAlt(_localctx, 1);
				{
				setState(175);
				match(EQUAL);
				setState(176);
				((RelPContext)_localctx).add = add();
				setState(177);
				((RelPContext)_localctx).relP = relP(new AstBinExpr(new Location(_localctx.missing_ast.location(), ((RelPContext)_localctx).add.ast.location()), 
									AstBinExpr.Oper.EQU, _localctx.missing_ast, ((RelPContext)_localctx).add.ast));
				((RelPContext)_localctx).ast =  ((RelPContext)_localctx).relP.ast;
				}
				break;
			case NOTEQUAL:
				enterOuterAlt(_localctx, 2);
				{
				setState(180);
				match(NOTEQUAL);
				setState(181);
				((RelPContext)_localctx).add = add();
				setState(182);
				((RelPContext)_localctx).relP = relP(new AstBinExpr(new Location(_localctx.missing_ast.location(), ((RelPContext)_localctx).add.ast.location()), 
									AstBinExpr.Oper.NEQ, _localctx.missing_ast, ((RelPContext)_localctx).add.ast));
				((RelPContext)_localctx).ast =  ((RelPContext)_localctx).relP.ast;
				}
				break;
			case LT:
				enterOuterAlt(_localctx, 3);
				{
				setState(185);
				match(LT);
				setState(186);
				((RelPContext)_localctx).add = add();
				setState(187);
				((RelPContext)_localctx).relP = relP(new AstBinExpr(new Location(_localctx.missing_ast.location(), ((RelPContext)_localctx).add.ast.location()), 
									AstBinExpr.Oper.LTH, _localctx.missing_ast, ((RelPContext)_localctx).add.ast));
				((RelPContext)_localctx).ast =  ((RelPContext)_localctx).relP.ast;
				}
				break;
			case GT:
				enterOuterAlt(_localctx, 4);
				{
				setState(190);
				match(GT);
				setState(191);
				((RelPContext)_localctx).add = add();
				setState(192);
				((RelPContext)_localctx).relP = relP(new AstBinExpr(new Location(_localctx.missing_ast.location(), ((RelPContext)_localctx).add.ast.location()), 
									AstBinExpr.Oper.GTH, _localctx.missing_ast, ((RelPContext)_localctx).add.ast));
				((RelPContext)_localctx).ast =  ((RelPContext)_localctx).relP.ast;
				}
				break;
			case LE:
				enterOuterAlt(_localctx, 5);
				{
				setState(195);
				match(LE);
				setState(196);
				((RelPContext)_localctx).add = add();
				setState(197);
				((RelPContext)_localctx).relP = relP(new AstBinExpr(new Location(_localctx.missing_ast.location(), ((RelPContext)_localctx).add.ast.location()), 
									AstBinExpr.Oper.LEQ, _localctx.missing_ast, ((RelPContext)_localctx).add.ast));
				((RelPContext)_localctx).ast =  ((RelPContext)_localctx).relP.ast;
				}
				break;
			case GE:
				enterOuterAlt(_localctx, 6);
				{
				setState(200);
				match(GE);
				setState(201);
				((RelPContext)_localctx).add = add();
				setState(202);
				((RelPContext)_localctx).relP = relP(new AstBinExpr(new Location(_localctx.missing_ast.location(), ((RelPContext)_localctx).add.ast.location()), 
									AstBinExpr.Oper.GEQ, _localctx.missing_ast, ((RelPContext)_localctx).add.ast));
				((RelPContext)_localctx).ast =  ((RelPContext)_localctx).relP.ast;
				}
				break;
			case RP:
			case RB:
			case COLON:
			case SEMICOLON:
			case COMA:
			case AND:
			case OR:
			case ASSIGN:
			case DO:
			case THEN:
			case WHERE:
				enterOuterAlt(_localctx, 7);
				{
				((RelPContext)_localctx).ast =  _localctx.missing_ast;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AddContext extends ParserRuleContext {
		public AstExpr ast;
		public MulContext mul;
		public AddPContext addP;
		public MulContext mul() {
			return getRuleContext(MulContext.class,0);
		}
		public AddPContext addP() {
			return getRuleContext(AddPContext.class,0);
		}
		public AddContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_add; }
	}

	public final AddContext add() throws RecognitionException {
		AddContext _localctx = new AddContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_add);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(208);
			((AddContext)_localctx).mul = mul();
			setState(209);
			((AddContext)_localctx).addP = addP(((AddContext)_localctx).mul.ast);
			((AddContext)_localctx).ast =  ((AddContext)_localctx).addP.ast;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AddPContext extends ParserRuleContext {
		public AstExpr missing_ast;
		public AstExpr ast;
		public MulContext mul;
		public AddPContext addP;
		public TerminalNode PLUS() { return getToken(PinsParser.PLUS, 0); }
		public MulContext mul() {
			return getRuleContext(MulContext.class,0);
		}
		public AddPContext addP() {
			return getRuleContext(AddPContext.class,0);
		}
		public TerminalNode MINUS() { return getToken(PinsParser.MINUS, 0); }
		public AddPContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public AddPContext(ParserRuleContext parent, int invokingState, AstExpr missing_ast) {
			super(parent, invokingState);
			this.missing_ast = missing_ast;
		}
		@Override public int getRuleIndex() { return RULE_addP; }
	}

	public final AddPContext addP(AstExpr missing_ast) throws RecognitionException {
		AddPContext _localctx = new AddPContext(_ctx, getState(), missing_ast);
		enterRule(_localctx, 24, RULE_addP);
		try {
			setState(223);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case PLUS:
				enterOuterAlt(_localctx, 1);
				{
				setState(212);
				match(PLUS);
				setState(213);
				((AddPContext)_localctx).mul = mul();
				setState(214);
				((AddPContext)_localctx).addP = addP(new AstBinExpr(new Location(_localctx.missing_ast.location(), ((AddPContext)_localctx).mul.ast.location()), 
									 AstBinExpr.Oper.ADD, _localctx.missing_ast, ((AddPContext)_localctx).mul.ast));
				((AddPContext)_localctx).ast =  ((AddPContext)_localctx).addP.ast;
				}
				break;
			case MINUS:
				enterOuterAlt(_localctx, 2);
				{
				setState(217);
				match(MINUS);
				setState(218);
				((AddPContext)_localctx).mul = mul();
				setState(219);
				((AddPContext)_localctx).addP = addP(new AstBinExpr(new Location(_localctx.missing_ast.location(), ((AddPContext)_localctx).mul.ast.location()), 
									AstBinExpr.Oper.SUB, _localctx.missing_ast, ((AddPContext)_localctx).mul.ast));
				((AddPContext)_localctx).ast =  ((AddPContext)_localctx).addP.ast;
				}
				break;
			case RP:
			case RB:
			case COLON:
			case SEMICOLON:
			case COMA:
			case AND:
			case OR:
			case EQUAL:
			case NOTEQUAL:
			case LT:
			case GT:
			case LE:
			case GE:
			case ASSIGN:
			case DO:
			case THEN:
			case WHERE:
				enterOuterAlt(_localctx, 3);
				{
				((AddPContext)_localctx).ast =  _localctx.missing_ast;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MulContext extends ParserRuleContext {
		public AstExpr ast;
		public PrexContext prex;
		public MulPContext mulP;
		public PrexContext prex() {
			return getRuleContext(PrexContext.class,0);
		}
		public MulPContext mulP() {
			return getRuleContext(MulPContext.class,0);
		}
		public MulContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mul; }
	}

	public final MulContext mul() throws RecognitionException {
		MulContext _localctx = new MulContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_mul);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(225);
			((MulContext)_localctx).prex = prex();
			setState(226);
			((MulContext)_localctx).mulP = mulP(((MulContext)_localctx).prex.ast);
			((MulContext)_localctx).ast =  ((MulContext)_localctx).mulP.ast;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MulPContext extends ParserRuleContext {
		public AstExpr missing_ast;
		public AstExpr ast;
		public PrexContext prex;
		public MulPContext mulP;
		public TerminalNode MUL() { return getToken(PinsParser.MUL, 0); }
		public PrexContext prex() {
			return getRuleContext(PrexContext.class,0);
		}
		public MulPContext mulP() {
			return getRuleContext(MulPContext.class,0);
		}
		public TerminalNode DIVIDE() { return getToken(PinsParser.DIVIDE, 0); }
		public TerminalNode MOD() { return getToken(PinsParser.MOD, 0); }
		public MulPContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public MulPContext(ParserRuleContext parent, int invokingState, AstExpr missing_ast) {
			super(parent, invokingState);
			this.missing_ast = missing_ast;
		}
		@Override public int getRuleIndex() { return RULE_mulP; }
	}

	public final MulPContext mulP(AstExpr missing_ast) throws RecognitionException {
		MulPContext _localctx = new MulPContext(_ctx, getState(), missing_ast);
		enterRule(_localctx, 28, RULE_mulP);
		try {
			setState(245);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case MUL:
				enterOuterAlt(_localctx, 1);
				{
				setState(229);
				match(MUL);
				setState(230);
				((MulPContext)_localctx).prex = prex();
				setState(231);
				((MulPContext)_localctx).mulP = mulP(new AstBinExpr(new Location(_localctx.missing_ast.location(), ((MulPContext)_localctx).prex.ast.location()), 
							AstBinExpr.Oper.MUL, _localctx.missing_ast, ((MulPContext)_localctx).prex.ast));
				((MulPContext)_localctx).ast =  ((MulPContext)_localctx).mulP.ast;
				}
				break;
			case DIVIDE:
				enterOuterAlt(_localctx, 2);
				{
				setState(234);
				match(DIVIDE);
				setState(235);
				((MulPContext)_localctx).prex = prex();
				setState(236);
				((MulPContext)_localctx).mulP = mulP(new AstBinExpr(new Location(_localctx.missing_ast.location(), ((MulPContext)_localctx).prex.ast.location()), 
							AstBinExpr.Oper.DIV, _localctx.missing_ast, ((MulPContext)_localctx).prex.ast));
				((MulPContext)_localctx).ast =  ((MulPContext)_localctx).mulP.ast;
				}
				break;
			case MOD:
				enterOuterAlt(_localctx, 3);
				{
				setState(239);
				match(MOD);
				setState(240);
				((MulPContext)_localctx).prex = prex();
				setState(241);
				((MulPContext)_localctx).mulP = mulP(new AstBinExpr(new Location(_localctx.missing_ast.location(), ((MulPContext)_localctx).prex.ast.location()), 
							AstBinExpr.Oper.MOD, _localctx.missing_ast, ((MulPContext)_localctx).prex.ast));
				((MulPContext)_localctx).ast =  ((MulPContext)_localctx).mulP.ast;
				}
				break;
			case RP:
			case RB:
			case COLON:
			case SEMICOLON:
			case COMA:
			case AND:
			case OR:
			case EQUAL:
			case NOTEQUAL:
			case LT:
			case GT:
			case LE:
			case GE:
			case PLUS:
			case MINUS:
			case ASSIGN:
			case DO:
			case THEN:
			case WHERE:
				enterOuterAlt(_localctx, 4);
				{
				((MulPContext)_localctx).ast =  _localctx.missing_ast;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrexContext extends ParserRuleContext {
		public AstExpr ast;
		public Token MINUS;
		public PrexContext prex;
		public Token PLUS;
		public Token NEGATE;
		public Token POW;
		public PfxContext pfx;
		public TerminalNode MINUS() { return getToken(PinsParser.MINUS, 0); }
		public PrexContext prex() {
			return getRuleContext(PrexContext.class,0);
		}
		public TerminalNode PLUS() { return getToken(PinsParser.PLUS, 0); }
		public TerminalNode NEGATE() { return getToken(PinsParser.NEGATE, 0); }
		public TerminalNode POW() { return getToken(PinsParser.POW, 0); }
		public PfxContext pfx() {
			return getRuleContext(PfxContext.class,0);
		}
		public PrexContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prex; }
	}

	public final PrexContext prex() throws RecognitionException {
		PrexContext _localctx = new PrexContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_prex);
		try {
			setState(266);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case MINUS:
				enterOuterAlt(_localctx, 1);
				{
				setState(247);
				((PrexContext)_localctx).MINUS = match(MINUS);
				setState(248);
				((PrexContext)_localctx).prex = prex();
				((PrexContext)_localctx).ast =  new AstPfxExpr(new Location((LexAn.PrevToken)((PrexContext)_localctx).MINUS, ((PrexContext)_localctx).prex.ast.location()), 
						AstPfxExpr.Oper.SUB, ((PrexContext)_localctx).prex.ast);
				}
				break;
			case PLUS:
				enterOuterAlt(_localctx, 2);
				{
				setState(251);
				((PrexContext)_localctx).PLUS = match(PLUS);
				setState(252);
				((PrexContext)_localctx).prex = prex();
				((PrexContext)_localctx).ast =  new AstPfxExpr(new Location((LexAn.PrevToken)((PrexContext)_localctx).PLUS, ((PrexContext)_localctx).prex.ast.location()), 
						AstPfxExpr.Oper.ADD, ((PrexContext)_localctx).prex.ast);
				}
				break;
			case NEGATE:
				enterOuterAlt(_localctx, 3);
				{
				setState(255);
				((PrexContext)_localctx).NEGATE = match(NEGATE);
				setState(256);
				((PrexContext)_localctx).prex = prex();
				((PrexContext)_localctx).ast =  new AstPfxExpr(new Location((LexAn.PrevToken)((PrexContext)_localctx).NEGATE, ((PrexContext)_localctx).prex.ast.location()), 
						AstPfxExpr.Oper.NOT, ((PrexContext)_localctx).prex.ast);
				}
				break;
			case POW:
				enterOuterAlt(_localctx, 4);
				{
				setState(259);
				((PrexContext)_localctx).POW = match(POW);
				setState(260);
				((PrexContext)_localctx).prex = prex();
				((PrexContext)_localctx).ast =  new AstPfxExpr(new Location((LexAn.PrevToken)((PrexContext)_localctx).POW, ((PrexContext)_localctx).prex.ast.location()), 
						AstPfxExpr.Oper.PTR, ((PrexContext)_localctx).prex.ast);
				}
				break;
			case LP:
			case NONE:
			case NIL:
			case ID:
			case INTCONST:
			case CHARCONST:
				enterOuterAlt(_localctx, 5);
				{
				setState(263);
				((PrexContext)_localctx).pfx = pfx();
				((PrexContext)_localctx).ast =  ((PrexContext)_localctx).pfx.ast;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PfxContext extends ParserRuleContext {
		public AstExpr ast;
		public EndExprContext endExpr;
		public PfxPContext pfxP;
		public EndExprContext endExpr() {
			return getRuleContext(EndExprContext.class,0);
		}
		public PfxPContext pfxP() {
			return getRuleContext(PfxPContext.class,0);
		}
		public PfxContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pfx; }
	}

	public final PfxContext pfx() throws RecognitionException {
		PfxContext _localctx = new PfxContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_pfx);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(268);
			((PfxContext)_localctx).endExpr = endExpr();
			setState(269);
			((PfxContext)_localctx).pfxP = pfxP(((PfxContext)_localctx).endExpr.ast);
			((PfxContext)_localctx).ast =  ((PfxContext)_localctx).pfxP.ast;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PfxPContext extends ParserRuleContext {
		public AstExpr missing_ast;
		public AstExpr ast;
		public ExprContext expr;
		public Token RB;
		public PfxPContext pfxP;
		public Token POW;
		public TerminalNode LB() { return getToken(PinsParser.LB, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode RB() { return getToken(PinsParser.RB, 0); }
		public PfxPContext pfxP() {
			return getRuleContext(PfxPContext.class,0);
		}
		public TerminalNode POW() { return getToken(PinsParser.POW, 0); }
		public PfxPContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PfxPContext(ParserRuleContext parent, int invokingState, AstExpr missing_ast) {
			super(parent, invokingState);
			this.missing_ast = missing_ast;
		}
		@Override public int getRuleIndex() { return RULE_pfxP; }
	}

	public final PfxPContext pfxP(AstExpr missing_ast) throws RecognitionException {
		PfxPContext _localctx = new PfxPContext(_ctx, getState(), missing_ast);
		enterRule(_localctx, 34, RULE_pfxP);
		try {
			setState(283);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LB:
				enterOuterAlt(_localctx, 1);
				{
				setState(272);
				match(LB);
				setState(273);
				((PfxPContext)_localctx).expr = expr();
				setState(274);
				((PfxPContext)_localctx).RB = match(RB);
				setState(275);
				((PfxPContext)_localctx).pfxP = pfxP(new AstArrExpr(new Location(_localctx.missing_ast.location(), (LexAn.PrevToken)((PfxPContext)_localctx).RB), 
							((PfxPContext)_localctx).expr.ast, _localctx.missing_ast));
				((PfxPContext)_localctx).ast =  ((PfxPContext)_localctx).pfxP.ast;
				}
				break;
			case POW:
				enterOuterAlt(_localctx, 2);
				{
				setState(278);
				((PfxPContext)_localctx).POW = match(POW);
				setState(279);
				((PfxPContext)_localctx).pfxP = pfxP(new AstSfxExpr(new Location((LexAn.PrevToken)((PfxPContext)_localctx).POW), 
							AstSfxExpr.Oper.PTR, _localctx.missing_ast));
				((PfxPContext)_localctx).ast =  ((PfxPContext)_localctx).pfxP.ast;
				}
				break;
			case RP:
			case RB:
			case COLON:
			case SEMICOLON:
			case COMA:
			case AND:
			case OR:
			case EQUAL:
			case NOTEQUAL:
			case LT:
			case GT:
			case LE:
			case GE:
			case MUL:
			case DIVIDE:
			case MOD:
			case PLUS:
			case MINUS:
			case ASSIGN:
			case DO:
			case THEN:
			case WHERE:
				enterOuterAlt(_localctx, 3);
				{
				((PfxPContext)_localctx).ast =  _localctx.missing_ast;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EndExprContext extends ParserRuleContext {
		public AstExpr ast;
		public Token INTCONST;
		public Token CHARCONST;
		public Token NIL;
		public Token NONE;
		public Token ID;
		public ExprContext ep1;
		public ExprContext ep2;
		public Token RP;
		public Token LP;
		public ExprContext expr;
		public TypeContext type;
		public SourceContext source;
		public TerminalNode INTCONST() { return getToken(PinsParser.INTCONST, 0); }
		public TerminalNode CHARCONST() { return getToken(PinsParser.CHARCONST, 0); }
		public TerminalNode NIL() { return getToken(PinsParser.NIL, 0); }
		public TerminalNode NONE() { return getToken(PinsParser.NONE, 0); }
		public TerminalNode ID() { return getToken(PinsParser.ID, 0); }
		public TerminalNode LP() { return getToken(PinsParser.LP, 0); }
		public TerminalNode RP() { return getToken(PinsParser.RP, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<TerminalNode> COMA() { return getTokens(PinsParser.COMA); }
		public TerminalNode COMA(int i) {
			return getToken(PinsParser.COMA, i);
		}
		public TerminalNode COLON() { return getToken(PinsParser.COLON, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode WHERE() { return getToken(PinsParser.WHERE, 0); }
		public SourceContext source() {
			return getRuleContext(SourceContext.class,0);
		}
		public EndExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_endExpr; }
	}

	public final EndExprContext endExpr() throws RecognitionException {
		EndExprContext _localctx = new EndExprContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_endExpr);
		int _la;
		try {
			setState(332);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,17,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(285);
				((EndExprContext)_localctx).INTCONST = match(INTCONST);
				((EndExprContext)_localctx).ast =  new AstAtomExpr(new Location((LexAn.PrevToken)((EndExprContext)_localctx).INTCONST), AstAtomExpr.Type.INT, ((EndExprContext)_localctx).INTCONST.getText());
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(287);
				((EndExprContext)_localctx).CHARCONST = match(CHARCONST);
				((EndExprContext)_localctx).ast =  new AstAtomExpr(new Location((LexAn.PrevToken)((EndExprContext)_localctx).CHARCONST), AstAtomExpr.Type.CHAR, ((EndExprContext)_localctx).CHARCONST.getText());
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(289);
				((EndExprContext)_localctx).NIL = match(NIL);
				((EndExprContext)_localctx).ast =  new AstAtomExpr(new Location((LexAn.PrevToken)((EndExprContext)_localctx).NIL), AstAtomExpr.Type.POINTER, ((EndExprContext)_localctx).NIL.getText());
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(291);
				((EndExprContext)_localctx).NONE = match(NONE);
				((EndExprContext)_localctx).ast =  new AstAtomExpr(new Location((LexAn.PrevToken)((EndExprContext)_localctx).NONE), AstAtomExpr.Type.VOID, ((EndExprContext)_localctx).NONE.getText());
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				Vector vector = new Vector();
				setState(294);
				((EndExprContext)_localctx).ID = match(ID);
				setState(295);
				match(LP);
				setState(307);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LP) | (1L << LBR) | (1L << NEGATE) | (1L << PLUS) | (1L << MINUS) | (1L << POW) | (1L << DEL) | (1L << NEW) | (1L << NONE) | (1L << NIL) | (1L << ID) | (1L << INTCONST) | (1L << CHARCONST))) != 0)) {
					{
					setState(296);
					((EndExprContext)_localctx).ep1 = expr();
					vector.add(((EndExprContext)_localctx).ep1.ast);
					setState(304);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMA) {
						{
						{
						setState(298);
						match(COMA);
						setState(299);
						((EndExprContext)_localctx).ep2 = expr();
						vector.add(((EndExprContext)_localctx).ep2.ast);
						}
						}
						setState(306);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(309);
				((EndExprContext)_localctx).RP = match(RP);
				AstTrees<AstExpr> tree = new AstTrees(vector);
					((EndExprContext)_localctx).ast =  new AstCallExpr(new Location((LexAn.PrevToken)((EndExprContext)_localctx).ID, (LexAn.PrevToken)((EndExprContext)_localctx).RP), ((EndExprContext)_localctx).ID.getText(), tree);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(311);
				((EndExprContext)_localctx).ID = match(ID);
				((EndExprContext)_localctx).ast =  new AstNameExpr(new Location((LexAn.PrevToken)((EndExprContext)_localctx).ID, (LexAn.PrevToken)((EndExprContext)_localctx).ID), ((EndExprContext)_localctx).ID.getText());
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(313);
				((EndExprContext)_localctx).LP = match(LP);
				setState(314);
				((EndExprContext)_localctx).expr = expr();
				setState(315);
				((EndExprContext)_localctx).RP = match(RP);
					((EndExprContext)_localctx).expr.ast.relocate(new Location((LexAn.PrevToken)((EndExprContext)_localctx).LP, (LexAn.PrevToken)((EndExprContext)_localctx).RP));
						((EndExprContext)_localctx).ast =  ((EndExprContext)_localctx).expr.ast;
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(318);
				((EndExprContext)_localctx).LP = match(LP);
				setState(319);
				((EndExprContext)_localctx).expr = expr();
				setState(320);
				match(COLON);
				setState(321);
				((EndExprContext)_localctx).type = type();
				setState(322);
				((EndExprContext)_localctx).RP = match(RP);
				((EndExprContext)_localctx).ast =  new AstCastExpr(new Location((LexAn.PrevToken)((EndExprContext)_localctx).LP, (LexAn.PrevToken)((EndExprContext)_localctx).RP), ((EndExprContext)_localctx).expr.ast, ((EndExprContext)_localctx).type.ast);
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(325);
				((EndExprContext)_localctx).LP = match(LP);
				setState(326);
				((EndExprContext)_localctx).expr = expr();
				setState(327);
				match(WHERE);
				setState(328);
				((EndExprContext)_localctx).source = source();
				setState(329);
				((EndExprContext)_localctx).RP = match(RP);
				((EndExprContext)_localctx).ast =  new AstWhereExpr(new Location((LexAn.PrevToken)((EndExprContext)_localctx).LP, (LexAn.PrevToken)((EndExprContext)_localctx).RP), ((EndExprContext)_localctx).expr.ast, ((EndExprContext)_localctx).source.ast);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StmtContext extends ParserRuleContext {
		public AstStmt ast;
		public ExprContext ep1;
		public Token SEMICOLON;
		public ExprContext ep2;
		public Token IF;
		public ExprContext expr;
		public StmtContext st1;
		public StmtContext st2;
		public Token WHILE;
		public StmtContext stmt;
		public TerminalNode SEMICOLON() { return getToken(PinsParser.SEMICOLON, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode ASSIGN() { return getToken(PinsParser.ASSIGN, 0); }
		public TerminalNode IF() { return getToken(PinsParser.IF, 0); }
		public TerminalNode THEN() { return getToken(PinsParser.THEN, 0); }
		public TerminalNode END() { return getToken(PinsParser.END, 0); }
		public TerminalNode ELSE() { return getToken(PinsParser.ELSE, 0); }
		public List<StmtContext> stmt() {
			return getRuleContexts(StmtContext.class);
		}
		public StmtContext stmt(int i) {
			return getRuleContext(StmtContext.class,i);
		}
		public TerminalNode WHILE() { return getToken(PinsParser.WHILE, 0); }
		public TerminalNode DO() { return getToken(PinsParser.DO, 0); }
		public StmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stmt; }
	}

	public final StmtContext stmt() throws RecognitionException {
		StmtContext _localctx = new StmtContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_stmt);
		int _la;
		try {
			setState(384);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,22,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(334);
				((StmtContext)_localctx).ep1 = expr();
				setState(335);
				((StmtContext)_localctx).SEMICOLON = match(SEMICOLON);
				((StmtContext)_localctx).ast =  new AstExprStmt(new Location(((StmtContext)_localctx).ep1.ast.location(), (LexAn.PrevToken)((StmtContext)_localctx).SEMICOLON), ((StmtContext)_localctx).ep1.ast);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(338);
				((StmtContext)_localctx).ep1 = expr();
				setState(339);
				match(ASSIGN);
				setState(340);
				((StmtContext)_localctx).ep2 = expr();
				setState(341);
				((StmtContext)_localctx).SEMICOLON = match(SEMICOLON);
				((StmtContext)_localctx).ast =  new AstAssignStmt(new Location(((StmtContext)_localctx).ep1.ast.location(), (LexAn.PrevToken)((StmtContext)_localctx).SEMICOLON), ((StmtContext)_localctx).ep1.ast, ((StmtContext)_localctx).ep2.ast);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
					Vector vector_then = new Vector();
						Vector vector_else = new Vector();
				setState(345);
				((StmtContext)_localctx).IF = match(IF);
				setState(346);
				((StmtContext)_localctx).expr = expr();
				setState(347);
				match(THEN);
				setState(351); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(348);
					((StmtContext)_localctx).st1 = stmt();
					vector_then.add(((StmtContext)_localctx).st1.ast);
					}
					}
					setState(353); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LP) | (1L << LBR) | (1L << NEGATE) | (1L << PLUS) | (1L << MINUS) | (1L << POW) | (1L << DEL) | (1L << IF) | (1L << NEW) | (1L << WHILE) | (1L << NONE) | (1L << NIL) | (1L << ID) | (1L << INTCONST) | (1L << CHARCONST))) != 0) );
				setState(363);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==ELSE) {
					{
					setState(355);
					match(ELSE);
					setState(359); 
					_errHandler.sync(this);
					_la = _input.LA(1);
					do {
						{
						{
						setState(356);
						((StmtContext)_localctx).st2 = stmt();
						vector_else.add(((StmtContext)_localctx).st2.ast);
						}
						}
						setState(361); 
						_errHandler.sync(this);
						_la = _input.LA(1);
					} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LP) | (1L << LBR) | (1L << NEGATE) | (1L << PLUS) | (1L << MINUS) | (1L << POW) | (1L << DEL) | (1L << IF) | (1L << NEW) | (1L << WHILE) | (1L << NONE) | (1L << NIL) | (1L << ID) | (1L << INTCONST) | (1L << CHARCONST))) != 0) );
					}
				}

				setState(365);
				match(END);
				setState(366);
				((StmtContext)_localctx).SEMICOLON = match(SEMICOLON);
					AstTrees<AstStmt> tree_then = new AstTrees(vector_then);
						AstTrees<AstStmt> tree_else = new AstTrees(vector_else);
						((StmtContext)_localctx).ast =  new AstIfStmt(new Location((LexAn.PrevToken)((StmtContext)_localctx).IF, (LexAn.PrevToken)((StmtContext)_localctx).SEMICOLON), 
								((StmtContext)_localctx).expr.ast, tree_then, tree_else);
					
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
					Vector vector_body = new Vector();
				setState(370);
				((StmtContext)_localctx).WHILE = match(WHILE);
				setState(371);
				((StmtContext)_localctx).expr = expr();
				setState(372);
				match(DO);
				setState(376); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(373);
					((StmtContext)_localctx).stmt = stmt();
					vector_body.add(((StmtContext)_localctx).stmt.ast);
					}
					}
					setState(378); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LP) | (1L << LBR) | (1L << NEGATE) | (1L << PLUS) | (1L << MINUS) | (1L << POW) | (1L << DEL) | (1L << IF) | (1L << NEW) | (1L << WHILE) | (1L << NONE) | (1L << NIL) | (1L << ID) | (1L << INTCONST) | (1L << CHARCONST))) != 0) );
				setState(380);
				match(END);
				setState(381);
				((StmtContext)_localctx).SEMICOLON = match(SEMICOLON);
					AstTrees<AstStmt> body_tree = new AstTrees(vector_body);
						((StmtContext)_localctx).ast =  new AstWhileStmt(new Location((LexAn.PrevToken)((StmtContext)_localctx).WHILE, (LexAn.PrevToken)((StmtContext)_localctx).SEMICOLON),
						((StmtContext)_localctx).expr.ast, body_tree);
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\66\u0185\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\3\2\3\2\3\2\3\2\6\2/\n\2\r\2\16\2\60\3"+
		"\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\7\3Q\n\3\f\3\16\3T\13"+
		"\3\5\3V\n\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3`\n\3\3\4\3\4\3\4\3\4\3"+
		"\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4"+
		"\3\4\5\4y\n\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\6\5\u0087"+
		"\n\5\r\5\16\5\u0088\3\5\3\5\3\5\5\5\u008e\n\5\3\6\3\6\3\6\3\6\5\6\u0094"+
		"\n\6\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\5\b\u00a0\n\b\3\t\3\t\3\t"+
		"\3\t\3\n\3\n\3\n\3\n\3\n\3\n\5\n\u00ac\n\n\3\13\3\13\3\13\3\13\3\f\3\f"+
		"\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3"+
		"\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u00d1\n\f\3\r\3\r\3"+
		"\r\3\r\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\5\16\u00e2"+
		"\n\16\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20"+
		"\3\20\3\20\3\20\3\20\3\20\3\20\3\20\5\20\u00f8\n\20\3\21\3\21\3\21\3\21"+
		"\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21"+
		"\3\21\5\21\u010d\n\21\3\22\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3\23"+
		"\3\23\3\23\3\23\3\23\3\23\5\23\u011e\n\23\3\24\3\24\3\24\3\24\3\24\3\24"+
		"\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\7\24\u0131\n\24"+
		"\f\24\16\24\u0134\13\24\5\24\u0136\n\24\3\24\3\24\3\24\3\24\3\24\3\24"+
		"\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24"+
		"\3\24\3\24\3\24\5\24\u014f\n\24\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25"+
		"\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\6\25\u0162\n\25\r\25\16"+
		"\25\u0163\3\25\3\25\3\25\3\25\6\25\u016a\n\25\r\25\16\25\u016b\5\25\u016e"+
		"\n\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\6\25\u017b"+
		"\n\25\r\25\16\25\u017c\3\25\3\25\3\25\3\25\5\25\u0183\n\25\3\25\2\2\26"+
		"\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(\2\2\2\u01a3\2*\3\2\2\2\4"+
		"_\3\2\2\2\6x\3\2\2\2\b\u008d\3\2\2\2\n\u0093\3\2\2\2\f\u0095\3\2\2\2\16"+
		"\u009f\3\2\2\2\20\u00a1\3\2\2\2\22\u00ab\3\2\2\2\24\u00ad\3\2\2\2\26\u00d0"+
		"\3\2\2\2\30\u00d2\3\2\2\2\32\u00e1\3\2\2\2\34\u00e3\3\2\2\2\36\u00f7\3"+
		"\2\2\2 \u010c\3\2\2\2\"\u010e\3\2\2\2$\u011d\3\2\2\2&\u014e\3\2\2\2(\u0182"+
		"\3\2\2\2*.\b\2\1\2+,\5\4\3\2,-\b\2\1\2-/\3\2\2\2.+\3\2\2\2/\60\3\2\2\2"+
		"\60.\3\2\2\2\60\61\3\2\2\2\61\62\3\2\2\2\62\63\b\2\1\2\63\3\3\2\2\2\64"+
		"\65\7\'\2\2\65\66\7\60\2\2\66\67\7\34\2\2\678\5\6\4\289\7\13\2\29:\b\3"+
		"\1\2:`\3\2\2\2;<\7(\2\2<=\7\60\2\2=>\7\n\2\2>?\5\6\4\2?@\7\13\2\2@A\b"+
		"\3\1\2A`\3\2\2\2BC\b\3\1\2CD\7\"\2\2DE\7\60\2\2EU\7\3\2\2FG\7\60\2\2G"+
		"H\7\n\2\2HI\5\6\4\2IR\b\3\1\2JK\7\f\2\2KL\7\60\2\2LM\7\n\2\2MN\5\6\4\2"+
		"NO\b\3\1\2OQ\3\2\2\2PJ\3\2\2\2QT\3\2\2\2RP\3\2\2\2RS\3\2\2\2SV\3\2\2\2"+
		"TR\3\2\2\2UF\3\2\2\2UV\3\2\2\2VW\3\2\2\2WX\7\4\2\2XY\7\n\2\2YZ\5\6\4\2"+
		"Z[\7\34\2\2[\\\5\b\5\2\\]\7\13\2\2]^\b\3\1\2^`\3\2\2\2_\64\3\2\2\2_;\3"+
		"\2\2\2_B\3\2\2\2`\5\3\2\2\2ab\7)\2\2by\b\4\1\2cd\7\35\2\2dy\b\4\1\2ef"+
		"\7$\2\2fy\b\4\1\2gh\7\60\2\2hy\b\4\1\2ij\7\7\2\2jk\5\b\5\2kl\7\b\2\2l"+
		"m\5\6\4\2mn\b\4\1\2ny\3\2\2\2op\7\33\2\2pq\5\6\4\2qr\b\4\1\2ry\3\2\2\2"+
		"st\7\3\2\2tu\5\6\4\2uv\7\4\2\2vw\b\4\1\2wy\3\2\2\2xa\3\2\2\2xc\3\2\2\2"+
		"xe\3\2\2\2xg\3\2\2\2xi\3\2\2\2xo\3\2\2\2xs\3\2\2\2y\7\3\2\2\2z{\5\n\6"+
		"\2{|\5\b\5\2|}\b\5\1\2}\u008e\3\2\2\2~\177\5\f\7\2\177\u0080\b\5\1\2\u0080"+
		"\u008e\3\2\2\2\u0081\u0082\b\5\1\2\u0082\u0086\7\5\2\2\u0083\u0084\5("+
		"\25\2\u0084\u0085\b\5\1\2\u0085\u0087\3\2\2\2\u0086\u0083\3\2\2\2\u0087"+
		"\u0088\3\2\2\2\u0088\u0086\3\2\2\2\u0088\u0089\3\2\2\2\u0089\u008a\3\2"+
		"\2\2\u008a\u008b\7\6\2\2\u008b\u008c\b\5\1\2\u008c\u008e\3\2\2\2\u008d"+
		"z\3\2\2\2\u008d~\3\2\2\2\u008d\u0081\3\2\2\2\u008e\t\3\2\2\2\u008f\u0090"+
		"\7%\2\2\u0090\u0094\b\6\1\2\u0091\u0092\7\36\2\2\u0092\u0094\b\6\1\2\u0093"+
		"\u008f\3\2\2\2\u0093\u0091\3\2\2\2\u0094\13\3\2\2\2\u0095\u0096\5\20\t"+
		"\2\u0096\u0097\5\16\b\2\u0097\u0098\b\7\1\2\u0098\r\3\2\2\2\u0099\u009a"+
		"\7\16\2\2\u009a\u009b\5\20\t\2\u009b\u009c\5\16\b\2\u009c\u009d\b\b\1"+
		"\2\u009d\u00a0\3\2\2\2\u009e\u00a0\b\b\1\2\u009f\u0099\3\2\2\2\u009f\u009e"+
		"\3\2\2\2\u00a0\17\3\2\2\2\u00a1\u00a2\5\24\13\2\u00a2\u00a3\5\22\n\2\u00a3"+
		"\u00a4\b\t\1\2\u00a4\21\3\2\2\2\u00a5\u00a6\7\r\2\2\u00a6\u00a7\5\24\13"+
		"\2\u00a7\u00a8\5\22\n\2\u00a8\u00a9\b\n\1\2\u00a9\u00ac\3\2\2\2\u00aa"+
		"\u00ac\b\n\1\2\u00ab\u00a5\3\2\2\2\u00ab\u00aa\3\2\2\2\u00ac\23\3\2\2"+
		"\2\u00ad\u00ae\5\30\r\2\u00ae\u00af\5\26\f\2\u00af\u00b0\b\13\1\2\u00b0"+
		"\25\3\2\2\2\u00b1\u00b2\7\20\2\2\u00b2\u00b3\5\30\r\2\u00b3\u00b4\5\26"+
		"\f\2\u00b4\u00b5\b\f\1\2\u00b5\u00d1\3\2\2\2\u00b6\u00b7\7\21\2\2\u00b7"+
		"\u00b8\5\30\r\2\u00b8\u00b9\5\26\f\2\u00b9\u00ba\b\f\1\2\u00ba\u00d1\3"+
		"\2\2\2\u00bb\u00bc\7\22\2\2\u00bc\u00bd\5\30\r\2\u00bd\u00be\5\26\f\2"+
		"\u00be\u00bf\b\f\1\2\u00bf\u00d1\3\2\2\2\u00c0\u00c1\7\23\2\2\u00c1\u00c2"+
		"\5\30\r\2\u00c2\u00c3\5\26\f\2\u00c3\u00c4\b\f\1\2\u00c4\u00d1\3\2\2\2"+
		"\u00c5\u00c6\7\24\2\2\u00c6\u00c7\5\30\r\2\u00c7\u00c8\5\26\f\2\u00c8"+
		"\u00c9\b\f\1\2\u00c9\u00d1\3\2\2\2\u00ca\u00cb\7\25\2\2\u00cb\u00cc\5"+
		"\30\r\2\u00cc\u00cd\5\26\f\2\u00cd\u00ce\b\f\1\2\u00ce\u00d1\3\2\2\2\u00cf"+
		"\u00d1\b\f\1\2\u00d0\u00b1\3\2\2\2\u00d0\u00b6\3\2\2\2\u00d0\u00bb\3\2"+
		"\2\2\u00d0\u00c0\3\2\2\2\u00d0\u00c5\3\2\2\2\u00d0\u00ca\3\2\2\2\u00d0"+
		"\u00cf\3\2\2\2\u00d1\27\3\2\2\2\u00d2\u00d3\5\34\17\2\u00d3\u00d4\5\32"+
		"\16\2\u00d4\u00d5\b\r\1\2\u00d5\31\3\2\2\2\u00d6\u00d7\7\31\2\2\u00d7"+
		"\u00d8\5\34\17\2\u00d8\u00d9\5\32\16\2\u00d9\u00da\b\16\1\2\u00da\u00e2"+
		"\3\2\2\2\u00db\u00dc\7\32\2\2\u00dc\u00dd\5\34\17\2\u00dd\u00de\5\32\16"+
		"\2\u00de\u00df\b\16\1\2\u00df\u00e2\3\2\2\2\u00e0\u00e2\b\16\1\2\u00e1"+
		"\u00d6\3\2\2\2\u00e1\u00db\3\2\2\2\u00e1\u00e0\3\2\2\2\u00e2\33\3\2\2"+
		"\2\u00e3\u00e4\5 \21\2\u00e4\u00e5\5\36\20\2\u00e5\u00e6\b\17\1\2\u00e6"+
		"\35\3\2\2\2\u00e7\u00e8\7\26\2\2\u00e8\u00e9\5 \21\2\u00e9\u00ea\5\36"+
		"\20\2\u00ea\u00eb\b\20\1\2\u00eb\u00f8\3\2\2\2\u00ec\u00ed\7\27\2\2\u00ed"+
		"\u00ee\5 \21\2\u00ee\u00ef\5\36\20\2\u00ef\u00f0\b\20\1\2\u00f0\u00f8"+
		"\3\2\2\2\u00f1\u00f2\7\30\2\2\u00f2\u00f3\5 \21\2\u00f3\u00f4\5\36\20"+
		"\2\u00f4\u00f5\b\20\1\2\u00f5\u00f8\3\2\2\2\u00f6\u00f8\b\20\1\2\u00f7"+
		"\u00e7\3\2\2\2\u00f7\u00ec\3\2\2\2\u00f7\u00f1\3\2\2\2\u00f7\u00f6\3\2"+
		"\2\2\u00f8\37\3\2\2\2\u00f9\u00fa\7\32\2\2\u00fa\u00fb\5 \21\2\u00fb\u00fc"+
		"\b\21\1\2\u00fc\u010d\3\2\2\2\u00fd\u00fe\7\31\2\2\u00fe\u00ff\5 \21\2"+
		"\u00ff\u0100\b\21\1\2\u0100\u010d\3\2\2\2\u0101\u0102\7\17\2\2\u0102\u0103"+
		"\5 \21\2\u0103\u0104\b\21\1\2\u0104\u010d\3\2\2\2\u0105\u0106\7\33\2\2"+
		"\u0106\u0107\5 \21\2\u0107\u0108\b\21\1\2\u0108\u010d\3\2\2\2\u0109\u010a"+
		"\5\"\22\2\u010a\u010b\b\21\1\2\u010b\u010d\3\2\2\2\u010c\u00f9\3\2\2\2"+
		"\u010c\u00fd\3\2\2\2\u010c\u0101\3\2\2\2\u010c\u0105\3\2\2\2\u010c\u0109"+
		"\3\2\2\2\u010d!\3\2\2\2\u010e\u010f\5&\24\2\u010f\u0110\5$\23\2\u0110"+
		"\u0111\b\22\1\2\u0111#\3\2\2\2\u0112\u0113\7\7\2\2\u0113\u0114\5\b\5\2"+
		"\u0114\u0115\7\b\2\2\u0115\u0116\5$\23\2\u0116\u0117\b\23\1\2\u0117\u011e"+
		"\3\2\2\2\u0118\u0119\7\33\2\2\u0119\u011a\5$\23\2\u011a\u011b\b\23\1\2"+
		"\u011b\u011e\3\2\2\2\u011c\u011e\b\23\1\2\u011d\u0112\3\2\2\2\u011d\u0118"+
		"\3\2\2\2\u011d\u011c\3\2\2\2\u011e%\3\2\2\2\u011f\u0120\7\61\2\2\u0120"+
		"\u014f\b\24\1\2\u0121\u0122\7\62\2\2\u0122\u014f\b\24\1\2\u0123\u0124"+
		"\7-\2\2\u0124\u014f\b\24\1\2\u0125\u0126\7,\2\2\u0126\u014f\b\24\1\2\u0127"+
		"\u0128\b\24\1\2\u0128\u0129\7\60\2\2\u0129\u0135\7\3\2\2\u012a\u012b\5"+
		"\b\5\2\u012b\u0132\b\24\1\2\u012c\u012d\7\f\2\2\u012d\u012e\5\b\5\2\u012e"+
		"\u012f\b\24\1\2\u012f\u0131\3\2\2\2\u0130\u012c\3\2\2\2\u0131\u0134\3"+
		"\2\2\2\u0132\u0130\3\2\2\2\u0132\u0133\3\2\2\2\u0133\u0136\3\2\2\2\u0134"+
		"\u0132\3\2\2\2\u0135\u012a\3\2\2\2\u0135\u0136\3\2\2\2\u0136\u0137\3\2"+
		"\2\2\u0137\u0138\7\4\2\2\u0138\u014f\b\24\1\2\u0139\u013a\7\60\2\2\u013a"+
		"\u014f\b\24\1\2\u013b\u013c\7\3\2\2\u013c\u013d\5\b\5\2\u013d\u013e\7"+
		"\4\2\2\u013e\u013f\b\24\1\2\u013f\u014f\3\2\2\2\u0140\u0141\7\3\2\2\u0141"+
		"\u0142\5\b\5\2\u0142\u0143\7\n\2\2\u0143\u0144\5\6\4\2\u0144\u0145\7\4"+
		"\2\2\u0145\u0146\b\24\1\2\u0146\u014f\3\2\2\2\u0147\u0148\7\3\2\2\u0148"+
		"\u0149\5\b\5\2\u0149\u014a\7*\2\2\u014a\u014b\5\2\2\2\u014b\u014c\7\4"+
		"\2\2\u014c\u014d\b\24\1\2\u014d\u014f\3\2\2\2\u014e\u011f\3\2\2\2\u014e"+
		"\u0121\3\2\2\2\u014e\u0123\3\2\2\2\u014e\u0125\3\2\2\2\u014e\u0127\3\2"+
		"\2\2\u014e\u0139\3\2\2\2\u014e\u013b\3\2\2\2\u014e\u0140\3\2\2\2\u014e"+
		"\u0147\3\2\2\2\u014f\'\3\2\2\2\u0150\u0151\5\b\5\2\u0151\u0152\7\13\2"+
		"\2\u0152\u0153\b\25\1\2\u0153\u0183\3\2\2\2\u0154\u0155\5\b\5\2\u0155"+
		"\u0156\7\34\2\2\u0156\u0157\5\b\5\2\u0157\u0158\7\13\2\2\u0158\u0159\b"+
		"\25\1\2\u0159\u0183\3\2\2\2\u015a\u015b\b\25\1\2\u015b\u015c\7#\2\2\u015c"+
		"\u015d\5\b\5\2\u015d\u0161\7&\2\2\u015e\u015f\5(\25\2\u015f\u0160\b\25"+
		"\1\2\u0160\u0162\3\2\2\2\u0161\u015e\3\2\2\2\u0162\u0163\3\2\2\2\u0163"+
		"\u0161\3\2\2\2\u0163\u0164\3\2\2\2\u0164\u016d\3\2\2\2\u0165\u0169\7 "+
		"\2\2\u0166\u0167\5(\25\2\u0167\u0168\b\25\1\2\u0168\u016a\3\2\2\2\u0169"+
		"\u0166\3\2\2\2\u016a\u016b\3\2\2\2\u016b\u0169\3\2\2\2\u016b\u016c\3\2"+
		"\2\2\u016c\u016e\3\2\2\2\u016d\u0165\3\2\2\2\u016d\u016e\3\2\2\2\u016e"+
		"\u016f\3\2\2\2\u016f\u0170\7!\2\2\u0170\u0171\7\13\2\2\u0171\u0172\b\25"+
		"\1\2\u0172\u0183\3\2\2\2\u0173\u0174\b\25\1\2\u0174\u0175\7+\2\2\u0175"+
		"\u0176\5\b\5\2\u0176\u017a\7\37\2\2\u0177\u0178\5(\25\2\u0178\u0179\b"+
		"\25\1\2\u0179\u017b\3\2\2\2\u017a\u0177\3\2\2\2\u017b\u017c\3\2\2\2\u017c"+
		"\u017a\3\2\2\2\u017c\u017d\3\2\2\2\u017d\u017e\3\2\2\2\u017e\u017f\7!"+
		"\2\2\u017f\u0180\7\13\2\2\u0180\u0181\b\25\1\2\u0181\u0183\3\2\2\2\u0182"+
		"\u0150\3\2\2\2\u0182\u0154\3\2\2\2\u0182\u015a\3\2\2\2\u0182\u0173\3\2"+
		"\2\2\u0183)\3\2\2\2\31\60RU_x\u0088\u008d\u0093\u009f\u00ab\u00d0\u00e1"+
		"\u00f7\u010c\u011d\u0132\u0135\u014e\u0163\u016b\u016d\u017c\u0182";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}