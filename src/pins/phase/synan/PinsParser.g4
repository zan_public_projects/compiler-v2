parser grammar PinsParser;

@header {

	package pins.phase.synan;
	
	import java.util.*;
	
	import pins.common.report.*;
	import pins.phase.lexan.*;
	import pins.data.ast.tree.*;
	import pins.data.ast.tree.decl.*;	
	import pins.data.ast.tree.expr.*;
	import pins.data.ast.tree.stmt.*;
	import pins.data.ast.tree.type.*;
}

@members {
	public int getLine(Token token){
		return token.getLine();
	}

	public int getStartCharPos(Token token){
		return token.getCharPositionInLine();
	}

	public int getEndCharPos(Token token){
		int temp = token.getCharPositionInLine() + token.getText().length() - 1;
		return temp;
	}
}

options{
    tokenVocab=PinsLexer;
}

// ID ASSIGN INTCONST SEMICOLON
/*source
	returns [AstTrees<AstDecl> ast]
  	: prg { $ast = $prg.ast; }
  	; */

source		// v bistvu prg
	// : decl decl*
	returns [AstTrees<AstDecl> ast]
	: { Vector decl = new Vector(); } ( decl { decl.add($decl.ast); } )+ { $ast = new AstTrees(decl); } 
	;

decl
	returns [AstDecl ast]
	: TYP ID ASSIGN type SEMICOLON 	
	{ $ast = new AstTypeDecl(new Location((LexAn.PrevToken)$TYP, (LexAn.PrevToken)$SEMICOLON), $ID.getText(), $type.ast); } 

	| VAR ID COLON type SEMICOLON
	{ $ast = new AstVarDecl(new Location((LexAn.PrevToken)$VAR, (LexAn.PrevToken)$SEMICOLON), $ID.getText(), $type.ast); } 

	| { Vector vector = new Vector(); }
	FUN i_main=ID LP 
		(i1=ID COLON ty_1=type  
		{ vector.add(new AstParDecl(new Location((LexAn.PrevToken)$i1, $ty_1.ast.location()), $i1.getText(), $ty_1.ast));}
	
				( COMA i2=ID COLON ty_2=type 
				{ vector.add(new AstParDecl(new Location((LexAn.PrevToken)$COMA, $ty_2.ast.location()), $i2.getText(), $ty_2.ast)); }
				)* 

		)? 
	
	RP COLON t1=type ASSIGN ex1=expr SEMICOLON {
		AstTrees<AstParDecl> tree = new AstTrees(vector);
		$ast = new AstFunDecl(new Location((LexAn.PrevToken)$FUN, (LexAn.PrevToken)$SEMICOLON), 
		$i_main.getText(), tree, $t1.ast, $ex1.ast);
	}
	; 



type
	returns [AstType ast]
	: VOID {$ast = new AstAtomType(new Location((LexAn.PrevToken)$VOID), AstAtomType.Type.VOID);}
	| CHAR {$ast = new AstAtomType(new Location((LexAn.PrevToken)$CHAR), AstAtomType.Type.CHAR);}
	| INT  {$ast = new AstAtomType(new Location((LexAn.PrevToken)$INT) , AstAtomType.Type.INT);}
	| ID   {$ast = new AstNameType(new Location((LexAn.PrevToken)$ID), $ID.getText());}
	| LB expr RB type  	
	{	$ast = new AstArrType(new Location((LexAn.PrevToken)$LB, (LexAn.PrevToken)$RB), $type.ast, $expr.ast);}
	
	| POW ty_1=type 
	{$ast = new AstPtrType(new Location((LexAn.PrevToken)$POW, $ty_1.ast.location()), $ty_1.ast);}
	| LP ty_1=type RP    
	{	$ty_1.ast.relocate(new Location((LexAn.PrevToken)$LP, (LexAn.PrevToken)$RP));
		$ast = $ty_1.ast;}	
	//{$ast = $ty_1.ast.relocate(new Location(new Location((LexAn.PrevToken)$LP, (LexAn.PrevToken)$RP)));}		
	//TODO fix this or ask prof.
	;	

expr 
	returns [AstExpr ast]
	: t1=pre end=expr	
	{$ast = new AstPfxExpr(new Location($t1.loc , $end.ast.location()), $pre.opr, $end.ast);}
	| dis				
	{$ast = $dis.ast;}
	// | LP expr COLON type RP			
	// {$ast = new AstCastExpr(new Location((LexAn.PrevToken)$LP, (LexAn.PrevToken)$RP), $expr.ast, $type.ast);}
	// | LP expr WHERE source RP		
	// {$ast = new AstWhereExpr(new Location((LexAn.PrevToken)$LP, (LexAn.PrevToken)$RP), $expr.ast, $source.ast);}		// changed decl to source
	/* | LP expr RP					
	{	$expr.ast.relocate(new Location((LexAn.PrevToken)$LP, (LexAn.PrevToken)$RP));
		$ast = $expr.ast;} */

	| { Vector vector = new Vector(); }
		LBR 
		( nes1=stmt { vector.add($nes1.ast); } )+ 
		RBR		
	{ AstTrees<AstStmt> tree = new AstTrees(vector);
		$ast = new AstStmtExpr(new Location((LexAn.PrevToken)$LBR, (LexAn.PrevToken)$RBR), tree);}
	;

pre 
	returns [AstPfxExpr.Oper opr, LexAn.PrevToken loc]
	: NEW {$opr = AstPfxExpr.Oper.NEW; $loc = (LexAn.PrevToken)$NEW;}
	| DEL {$opr = AstPfxExpr.Oper.DEL; $loc = (LexAn.PrevToken)$DEL;}
	;

dis 
	returns [AstExpr ast]
	: con disP[$con.ast]	{$ast = $disP.ast;}
	;

disP [AstExpr missing_ast]
	returns [AstExpr ast]
	: OR con disP[new AstBinExpr(new Location($missing_ast.location(), $con.ast.location()), 
				  AstBinExpr.Oper.OR, $missing_ast, $con.ast)] 
		{$ast = $disP.ast;}
	| {$ast = $missing_ast;}
	;	

con 
	returns [AstExpr ast]
	: rel conP[$rel.ast]	{$ast = $conP.ast;}
	;	

conP [AstExpr missing_ast]
	returns [AstExpr ast]
	: AND rel conP [new AstBinExpr(new Location($missing_ast.location(), $rel.ast.location()), 
				   AstBinExpr.Oper.AND, $missing_ast, $rel.ast)] 
	{$ast = $conP.ast;}
	| {$ast = $missing_ast;}
	;	

rel 
	returns [AstExpr ast]
	: add relP[$add.ast]	{$ast = $relP.ast;}
	;

relP [AstExpr missing_ast] 
	returns [AstExpr ast]
	: EQUAL add relP [new AstBinExpr(new Location($missing_ast.location(), $add.ast.location()), 
					AstBinExpr.Oper.EQU, $missing_ast, $add.ast)] 
	{$ast = $relP.ast;}

	| NOTEQUAL add relP [new AstBinExpr(new Location($missing_ast.location(), $add.ast.location()), 
					AstBinExpr.Oper.NEQ, $missing_ast, $add.ast)] 
	{$ast = $relP.ast;} 

	| LT add relP [new AstBinExpr(new Location($missing_ast.location(), $add.ast.location()), 
					AstBinExpr.Oper.LTH, $missing_ast, $add.ast)] 
	{$ast = $relP.ast;} 

	| GT add relP [new AstBinExpr(new Location($missing_ast.location(), $add.ast.location()), 
					AstBinExpr.Oper.GTH, $missing_ast, $add.ast)] 
	{$ast = $relP.ast;} 

	| LE add relP [new AstBinExpr(new Location($missing_ast.location(), $add.ast.location()), 
					AstBinExpr.Oper.LEQ, $missing_ast, $add.ast)] 
	{$ast = $relP.ast;} 

	| GE add relP [new AstBinExpr(new Location($missing_ast.location(), $add.ast.location()), 
					AstBinExpr.Oper.GEQ, $missing_ast, $add.ast)] 
	{$ast = $relP.ast;} 

	| {$ast = $missing_ast;}
	;

add 
	returns [AstExpr ast]
	: mul addP[$mul.ast]	{$ast = $addP.ast;}
	;

addP [AstExpr missing_ast]
	returns [AstExpr ast]
	: PLUS mul addP [new AstBinExpr(new Location($missing_ast.location(), $mul.ast.location()), 
					 AstBinExpr.Oper.ADD, $missing_ast, $mul.ast)] 
	{$ast = $addP.ast;}
	| MINUS mul addP [new AstBinExpr(new Location($missing_ast.location(), $mul.ast.location()), 
					AstBinExpr.Oper.SUB, $missing_ast, $mul.ast)] 
	{$ast = $addP.ast;}
	| {$ast = $missing_ast;}
	;

mul 
	returns [AstExpr ast]
	: prex mulP[$prex.ast] 	{$ast = $mulP.ast;}
	;

mulP [AstExpr missing_ast]
	returns [AstExpr ast]
	: MUL prex mulP [new AstBinExpr(new Location($missing_ast.location(), $prex.ast.location()), 
			AstBinExpr.Oper.MUL, $missing_ast, $prex.ast)] 
	{$ast = $mulP.ast;}
	| DIVIDE prex mulP [new AstBinExpr(new Location($missing_ast.location(), $prex.ast.location()), 
			AstBinExpr.Oper.DIV, $missing_ast, $prex.ast)] 
	{$ast = $mulP.ast;}
	| MOD prex mulP [new AstBinExpr(new Location($missing_ast.location(), $prex.ast.location()), 
			AstBinExpr.Oper.MOD, $missing_ast, $prex.ast)] 
	{$ast = $mulP.ast;}
	| {$ast = $missing_ast;}
	;

prex 
	returns [AstExpr ast]
	: MINUS prex	{$ast = new AstPfxExpr(new Location((LexAn.PrevToken)$MINUS, $prex.ast.location()), 
		AstPfxExpr.Oper.SUB, $prex.ast);}

	| PLUS prex {$ast = new AstPfxExpr(new Location((LexAn.PrevToken)$PLUS, $prex.ast.location()), 
		AstPfxExpr.Oper.ADD, $prex.ast);}

	| NEGATE prex
	{$ast = new AstPfxExpr(new Location((LexAn.PrevToken)$NEGATE, $prex.ast.location()), 
		AstPfxExpr.Oper.NOT, $prex.ast);}

	| POW prex  {$ast = new AstPfxExpr(new Location((LexAn.PrevToken)$POW, $prex.ast.location()), 
		AstPfxExpr.Oper.PTR, $prex.ast);}

	| pfx		{$ast = $pfx.ast;} 	// | 
	;


pfx 
	returns [AstExpr ast]
	: endExpr pfxP[$endExpr.ast] 	{$ast = $pfxP.ast;}
	;

pfxP [AstExpr missing_ast]
	returns [AstExpr ast]
	: LB expr RB pfxP [new AstArrExpr(new Location($missing_ast.location(), (LexAn.PrevToken)$RB), 
			$expr.ast, $missing_ast)] 
	{$ast = $pfxP.ast;}
	//TODO check location if ok;

	| POW pfxP [new AstSfxExpr(new Location((LexAn.PrevToken)$POW), 
			AstSfxExpr.Oper.PTR, $missing_ast)] 
	{$ast = $pfxP.ast;}

	| {$ast = $missing_ast;}
	;


endExpr 
	returns [AstExpr ast]
	: INTCONST		{$ast = new AstAtomExpr(new Location((LexAn.PrevToken)$INTCONST), AstAtomExpr.Type.INT, $INTCONST.getText());}

	| CHARCONST 	{$ast = new AstAtomExpr(new Location((LexAn.PrevToken)$CHARCONST), AstAtomExpr.Type.CHAR, $CHARCONST.getText());}

	| NIL 			{$ast = new AstAtomExpr(new Location((LexAn.PrevToken)$NIL), AstAtomExpr.Type.POINTER, $NIL.getText());}

	// | VOID 			{$ast = new AstAtomExpr(new Location((LexAn.PrevToken)$VOID), AstAtomExpr.Type.VOID, $VOID.getText());}

	| NONE			{$ast = new AstAtomExpr(new Location((LexAn.PrevToken)$NONE), AstAtomExpr.Type.VOID, $NONE.getText());}

	| {Vector vector = new Vector();}
	ID LP ( ep1=expr {vector.add($ep1.ast);}
		( COMA ep2=expr 
			{vector.add($ep2.ast);}
		)* 
	
	)? 
	
	RP
	{AstTrees<AstExpr> tree = new AstTrees(vector);
	$ast = new AstCallExpr(new Location((LexAn.PrevToken)$ID, (LexAn.PrevToken)$RP), $ID.getText(), tree);}

	| ID			{$ast = new AstNameExpr(new Location((LexAn.PrevToken)$ID, (LexAn.PrevToken)$ID), $ID.getText());}
	| LP expr RP					
	{	$expr.ast.relocate(new Location((LexAn.PrevToken)$LP, (LexAn.PrevToken)$RP));
		$ast = $expr.ast;}

	| LP expr COLON type RP			
	{$ast = new AstCastExpr(new Location((LexAn.PrevToken)$LP, (LexAn.PrevToken)$RP), $expr.ast, $type.ast);}
	| LP expr WHERE source RP		
	{$ast = new AstWhereExpr(new Location((LexAn.PrevToken)$LP, (LexAn.PrevToken)$RP), $expr.ast, $source.ast);}		// changed decl to source
	;

stmt 
	returns [AstStmt ast]
	: ep1=expr SEMICOLON {$ast = new AstExprStmt(new Location($ep1.ast.location(), (LexAn.PrevToken)$SEMICOLON), $ep1.ast);}

	| ep1=expr ASSIGN ep2=expr SEMICOLON
	{$ast = new AstAssignStmt(new Location($ep1.ast.location(), (LexAn.PrevToken)$SEMICOLON), $ep1.ast, $ep2.ast);}

	| {	Vector vector_then = new Vector();
		Vector vector_else = new Vector();}
	IF expr THEN 
		( st1=stmt {vector_then.add($st1.ast);}
		)+ 
		( ELSE ( st2=stmt {vector_else.add($st2.ast);}
		)+ )? 

	END SEMICOLON
	{	AstTrees<AstStmt> tree_then = new AstTrees(vector_then);
		AstTrees<AstStmt> tree_else = new AstTrees(vector_else);
		$ast = new AstIfStmt(new Location((LexAn.PrevToken)$IF, (LexAn.PrevToken)$SEMICOLON), 
				$expr.ast, tree_then, tree_else);
	}

	| {	Vector vector_body = new Vector();}
	WHILE expr DO 
		( stmt 	{vector_body.add($stmt.ast);})+ 
	END SEMICOLON 
	{	AstTrees<AstStmt> body_tree = new AstTrees(vector_body);
		$ast = new AstWhileStmt(new Location((LexAn.PrevToken)$WHILE, (LexAn.PrevToken)$SEMICOLON),
		$expr.ast, body_tree);
	}
	;

	
/*

*/
