package pins.phase.memory;

import pins.data.ast.tree.*;
import pins.common.report.*;
import pins.data.ast.tree.decl.*;
import pins.data.ast.tree.expr.*;
import pins.data.ast.tree.type.*;
import pins.data.ast.tree.stmt.*;
import pins.data.ast.visitor.*;
import pins.data.semtype.*;
import pins.data.mem.*;
import pins.phase.seman.*;
import pins.common.report.*;

/**
 * Computing memory layout: frames and accesses.
 */
public class MemEvaluator extends AstFullVisitor<Object, MemEvaluator.FunContext> {

	/**
	 * Functional context, i.e., used when traversing function and building a new
	 * frame, parameter acceses and variable acceses.
	 */
	protected class FunContext {
		public int depth = 0;
		public long locsSize = 0;
		public long argsSize = 0;
		public long parsSize = new SemPointer(new SemVoid()).size();
	}

	// GENERAL PURPOSE

	@Override
	public Object visit(AstTrees<? extends AstTree> trees, MemEvaluator.FunContext ctx) {
		if (ctx == null)
			ctx = new FunContext();
		for (AstTree t : trees)
			if (t != null)
				t.accept(this, ctx);
		return null;
	}

	// TODO 
	@Override
    public Object visit(AstPtrType ptrType, FunContext funContext) {
		// System.out.println("pointer size " + ptrType.baseType());
		long size = (long)ptrType.baseType().accept(this, funContext);

        return size;
	}

	@Override
	public Object visit(AstVarDecl varDecl, FunContext funContext) {
		long size = (long) varDecl.type().accept(this, funContext);
		// System.out.println("varDecl " + varDecl.type() + " ddd " + varDecl.name() + " velikost " + size + " globina " + funContext.depth);

		if(funContext.depth == 0){
			Memory.accesses.put(varDecl, new MemAbsAccess(size, new MemLabel(varDecl.name())));
		}
		else if(funContext.depth > 0){
			funContext.locsSize += size;
			Memory.accesses.put(varDecl, new MemRelAccess(size, (-funContext.locsSize), funContext.depth));
		}
		else{
			throw new Report.Error(varDecl.location(), "Unexpected error when declaring variable !!");
		}

		return null;
	}

	@Override
    public Object visit(AstFunDecl funDecl, FunContext funContext) {
		long prevLocalSize = funContext.locsSize;
		long prevArgsSize = funContext.argsSize;
		MemLabel label = null;
		if(funContext.depth == 0){
			label = new MemLabel(funDecl.name());
		}
		else{
			label = new MemLabel();
		}

		funContext.depth += 1;
		funContext.locsSize = 0;
		funContext.argsSize = 0;
		funContext.parsSize = new SemPointer(new SemVoid()).size();
				
		for (AstTree tree : funDecl.pars()){
			long size_par = (long) tree.accept(this, funContext);
			funContext.parsSize += size_par;
		}
		funDecl.type().accept(this, funContext);
		funDecl.expr().accept(this, funContext);

		MemFrame fun_mem = new MemFrame(label, funContext.depth, funContext.locsSize, funContext.argsSize);
		Memory.frames.put(funDecl, fun_mem);

		funContext.depth -= 1;
		funContext.locsSize = prevLocalSize;
		funContext.argsSize = prevArgsSize;
        return null;
	}
	
	@Override
    public Object visit(AstTypeDecl typeDecl, FunContext funContext) {
		long size = (long) typeDecl.type().accept(this, funContext);
		// System.out.println("typeDecl " + typeDecl.name() + " dd  " + typeDecl.location() + " funCont " + funContext);
		
        return size;
	}

	@Override
	public Object visit(AstArrType arrType, FunContext funContext) {
		long base_size = (long) arrType.elemType().accept(this, funContext);
		arrType.numElems().accept(this, funContext);
		long num_of_el = Long.parseLong(((AstAtomExpr)arrType.numElems()).value());

		/* System.out.println("el type " + arrType.elemType() + " numel " + arrType.numElems());
		System.out.println("ARRRRAY " + base_size + " NUM " + num_of_el); */
		
		return base_size * num_of_el;
	}

	@Override
    public Object visit(AstNameType nameType, FunContext funContext) {
        AstTypeDecl typeDecl = ((AstTypeDecl)SemAn.declaredAt.get(nameType));
        return visit(typeDecl, funContext);
	}
	
	@Override
    public Object visit(AstNameExpr nameExpr, FunContext funContext) {       
        return null;
	}

	// parDecl
	@Override
    public Object visit(AstParDecl parDecl, FunContext funContext) {
		long size = (long) parDecl.type().accept(this, funContext);
		Memory.accesses.put(parDecl, new MemRelAccess(size, funContext.parsSize, funContext.depth));
		
        return size;
	}

	// stmt
	@Override
	public Object visit(AstStmtExpr stmtExpr, FunContext funContext) {
		for(AstTree tree : stmtExpr.stmts()){
			tree.accept(this, funContext);
		}
		
		return null;
	}

	// stmt
	@Override
	public Object visit(AstWhileStmt stmtWhile, FunContext funContext) {
		stmtWhile.cond().accept(this, funContext);

		for(AstTree tree : stmtWhile.bodyStmts()){
			tree.accept(this, funContext);
		}
		
		return null;
	}

	// stmt
	@Override
	public Object visit(AstAssignStmt assignStmt, FunContext funContext) {
		assignStmt.src().accept(this, funContext);
		assignStmt.dst().accept(this, funContext);
		
		return null;
	}

	@Override
	public Object visit(AstBinExpr binExpr, FunContext funContext) {
		binExpr.fstExpr().accept(this, funContext);
		binExpr.sndExpr().accept(this, funContext);
		
		return null;
	}

	// from the top baby
	@Override
	public Object visit(AstArrExpr arrExpr, FunContext funContext) {
		arrExpr.idx().accept(this, funContext);
		arrExpr.arr().accept(this, funContext);
		
		return null;
	}

	@Override
	public Object visit(AstCallExpr callExpr, FunContext funContext) {
		for(AstTree tree : callExpr.args()){
			tree.accept(this, funContext);
		}

		long current_args = (callExpr.args().size() * 8) + 8;
		if(current_args > funContext.argsSize){
			funContext.argsSize = current_args;
		}
		// funContext.argsSize += (callExpr.args().size() * 8) + 8;
		
		return null;
	}

	@Override
	public Object visit(AstCastExpr castExpr, FunContext funContext) {
		castExpr.expr().accept(this, funContext);
		castExpr.type().accept(this, funContext);
		
		return null;
	}

	@Override
	public Object visit(AstPfxExpr pfxExpr, FunContext funContext) {
		pfxExpr.expr().accept(this, funContext);
		
		return null;
	}

	@Override
	public Object visit(AstSfxExpr sfxExpr, FunContext funContext) {
		sfxExpr.expr().accept(this, funContext);
		
		return null;
	}
	
	@Override
	public Object visit(AstWhereExpr whereExpr, FunContext funContext) {
		whereExpr.expr().accept(this, funContext);
		whereExpr.decls().accept(this, funContext);
		
		return null;
	}

	// stmt
	@Override
	public Object visit(AstExprStmt exprStmt, FunContext funContext) {
		exprStmt.expr().accept(this, funContext);
		
		return null;
	}

	@Override
	public Object visit(AstIfStmt ifStmt, FunContext funContext) {
		ifStmt.cond().accept(this, funContext);

		for(AstTree tree : ifStmt.thenStmts()){
			tree.accept(this, funContext);
		}
		for(AstTree tree : ifStmt.elseStmts()){
			tree.accept(this, funContext);
		}
		
		return null;
	}


	@Override
    public Object visit(AstAtomExpr atomExpr, FunContext funContext) {
		// System.out.println("atomExpr -> int, char, void time");
        return 8L;
	}
	
	@Override
	public Object visit(AstAtomType atomType, FunContext funContext) {
		// System.out.println("atomType -> int, char, void time !!");
		return 8L;
	}
}
